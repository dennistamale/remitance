<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<?php

class site_options {
    private $CI;

    function __construct()
        {
            $this->CI =& get_instance();
            $this->CI->load->database();
        }


    function dennis()
        {
            //$this->CI
            $users = $this->CI->db->select()->from('users')->get()->result();
            return ($users);
        }
    function title($title=null){

        if(isset($title)) {

            $h = $this->CI->db->select()->from('site_options')->where(array('option_name' => underscore($title)))->get()->row();

            return count($h) == 1 ? $h->option_value : 'No value';
        }else{
            return 'missing parameter e.g <b>(title({title}))</b>';
        }

    }
}