<?php if($subtitle=='branch'){ ?>

<?php

$branch = $this->db->select()->from('branch')->where('id', $id)->get()->result();

?>

<table class="table table-bordered table-striped  table-hover">
    <tbody>
    <?php foreach ($branch as $t): ?>
        <tr class="success">

            <td>
                <span class="font-red bold">Summary</span></td>
            <td>
                Received <span style="font-size: xx-small">USD</span> :

                <?php
                $this->db->select_sum('received_amount_usd')->from('transactions')->where('receiver_branch', $t->id);
                $this->db->where(
                    array
                    (
                        'created_on >=' => $first_day,
                        'created_on <=' => $last_day,
                    )

                );
                $transfer = $this->db->get()->row();
                echo number_format($transfer->received_amount_usd, 2)
                ?>
            </td>


            <td>

                Commission <span style="font-size: xx-small">USD</span>

                <?php
                $this->db->select_sum('commission_usd')->from('transactions')->where(array('branch_id' => $t->id));
                $this->db->where(
                    array
                    (
                        'created_on >=' => $first_day,
                        'created_on <=' => $last_day,
                    )

                );
                $comm = $this->db->get()->row();
                echo number_format($comm->commission_usd, 3) ?>
            </td>

            <td>

                Sent Amt <span style="font-size: xx-small">USD</span> :

                <?php
                $this->db->select_sum('sent_amount_usd')->from('transactions')->where(array('branch_id' => $t->id));
                $this->db->where(
                    array
                    (
                        'created_on >=' => $first_day,
                        'created_on <=' => $last_day,
                    )

                );
                $sent_amount = $this->db->get()->row();
                echo number_format($sent_amount->sent_amount_usd, 2) ?>
            </td>



            <td>

                Cashedout <span style="font-size: xx-small">USD</span> :

                <?php
                $this->db->select_sum('received_amount_usd')->from('transactions')->where(array('receiver_branch' => $t->id, 'status' => 'cashed_out'));
                $this->db->where(
                    array
                    (
                        'created_on >=' => $first_day,
                        'created_on <=' => $last_day,
                    )

                );
                $cashout = $this->db->get()->row();
                echo number_format($cashout->received_amount_usd, 2) ?>
            </td>

            <td>

                Pending <span style="font-size: xx-small">USD</span> :
                <?php
                $this->db->select_sum('received_amount_usd')->from('transactions')->where(array('receiver_branch' => $t->id, 'status' => 'pending'));
                $this->db->where(
                    array
                    (
                        'created_on >=' => $first_day,
                        'created_on <=' => $last_day,
                    )

                );
                $pending = $this->db->get()->row();
                echo number_format($pending->received_amount_usd, 2) ?>
            </td>


            <td>

                Taxes <span style="font-size: xx-small">USD</span>

                <?php
                $this->db->select_sum('other_charges_usd')->from('transactions')->where(array('branch_id' => $t->id));
                $this->db->where(
                    array
                    (
                        'created_on >=' => $first_day,
                        'created_on <=' => $last_day,
                    )

                );
                $comm = $this->db->get()->row();
                echo number_format($comm->other_charges_usd, 3) ?>
            </td>



        </tr>
    <?php endforeach; ?>

    </tbody>
</table>


<?php }else{ ?>

<?php //print_r($t[0]);

    $received_amount_usd=0;
    $other_charges_usd=0;
    $commission_usd=0;
    $sent_amount_usd=0;


    foreach($t as $tr){

        $received_amount_usd=$received_amount_usd+$tr->received_amount_usd;
        $other_charges_usd=$other_charges_usd+$tr->other_charges_usd;
        $commission_usd=$commission_usd+$tr->commission_usd;
        $sent_amount_usd=$sent_amount_usd+$tr->sent_amount_usd;


    }

    ?>

    <table class="table table-bordered table-striped  table-hover">
        <tbody>
        <tr  class="success" style="font-weight: bold;">
            <td style="width: 30px; color: red;">Summary(Total)</td>
            <td>Net Amt : <?php echo number_format($received_amount_usd,2) ?> USD</td>
            <td>Commission : <?php echo number_format($commission_usd,2) ?> USD</td>
            <td>Taxes : <?php echo number_format($other_charges_usd,2)  ?> USD</td>

            <td>Gross Amt : <?php echo number_format($sent_amount_usd,2) ?> USD</td>
        </tr>
        </tbody>
    </table>

<?php } ?>