<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                            <h4 class="bold"><?php echo humanize($subtitle) ?> Report</h4>
                        </div>
                    </div>
                    <div class="col-md-6 hidden-print">
                        <div class="btn-group pull-right">
                            <button class="btn green  btn-outline " data-toggle="dropdown"  onclick="javascript:window.print();"><i class="fa fa-print"></i> Print
                            </button>

                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
               
               
                <table class="table table-scrollable table-bordered table-hover" cellspacing="0" width="100%" cellpadding="0">
                  

                  <tr>
                    <td>FXB NAME</td>
                    <td><?php echo $this->site_options->title('site_name') ?></td>
                    <td></td>
                    <td>Institution Code:</td>
                    <td><?php echo $this->site_options->title('central_bank_code') ?></td>
                    <td></td>
                    <td>Name</td>
                    <td> <?php echo $this->site_options->title('officer_name') ?></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>DAY</td>
                    <td><?php echo date('d') ?></td>
                    <td></td>
                    <td>Financial Year:</td>
                    <td><?php echo date('Y') ?></td>
                    <td></td>
                    <td>Position</td>
                    <td> <?php echo $this->site_options->title('officer_title') ?></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>MONTH</td>
                    <td><?php echo date('F') ?></td>
                    <td></td>
                    <td>Start Date:</td>
                    <td><?php echo '01-January-'.date('Y') ?></td>
                    <td></td>
                    <td>Phone Number</td>
                    <td> <?php echo $this->site_options->title('officer_phone') ?></td>
                    <td></td>
                  </tr>
                  <tr>
                    <td>YEAR</td>
                    <td><?php echo date('Y') ?></td>
                    <td></td>
                    <td>End Date:</td>
                    <td><?php echo date('d-F-Y') ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                  </tr>
                
                  <tr>
                    <td></td>
                    <td colspan="8" align="center" bgcolor="#99FF66"><strong>INFLOWS</strong></td>
                  </tr>
<!--                  <tr>-->
<!--                    <td>Product Name</td>-->
<!--                    <td>Total USD</td>-->
<!--                    <td>UGX Equivalent</td>-->
<!--                    <td>Total GBP</td>-->
<!--                    <td>UGX Equivalent</td>-->
<!--                    <td>Total EUR</td>-->
<!--                    <td>UGX Equivalent</td>-->
<!--                    <td>Total Others</td>-->
<!--                    <td>UGX equivalent</td>-->
<!--                  </tr>-->
                  <tr>

                    <td style="padding: 0;">
                      <table width="100%" class="table-bordered" border="0">

                        <tr style="font-weight: bold; border-bottom: solid 2px grey">
                          <td>
                           Month
                          </td>

                        </tr>


                        <?php
                      $c_motnth=date('n');
                        for($i='1';$i<=$c_motnth;$i++){ ?>
                          <tr>
                            <td>
                              <?php

                              $fdate=strtotime(date('Y-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01'));
                              echo $this_month= date('F Y',$fdate);

                              ?>
                            </td>

                          </tr>
                        <?php }
                        ?>





                      </table>
                    </td>
                    <td colspan="2" style="padding: 0;">
                    <?php
                    $current_country=$this->session->userdata('country');
                    $cc=$this->db->select('currency_alphabetic_code')->from('country')->where('a2_iso',$current_country)->get()->row();
                    $current_currency=$cc->currency_alphabetic_code;
                    ?>
                    <table width="100%" class="table-bordered" border="0">
<tr style="font-weight: bold; border-bottom: solid 2px grey;">
  <td>Total USD</td>
  <td align="right">UGX Equivalent</td>

</tr>



                          <?php

                          for($i='1';$i<=$c_motnth;$i++){ ?>
                      <tr>
                        <td align="right" width="40%">
                            <?php

                            $date=strtotime(date('Y-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01'));

                            $max_date=strtotime('last day of this month', $date);

                           $ra=$this->db->select_sum('received_amount')
                               ->from('transactions')

                               ->where(array(
                                 'created_on >='=>$date,
                                   'created_on <='=>$max_date,
                                   'recipient_currency'=>'USD'
                               ))
                               ->get()->row();
                            $usd_amount=$ra->received_amount;
                            echo number_format($usd_amount);

                            ?>
                        </td>

<!--                        this the Uganda Equivalent-->
                        <td align="right">
                          <?php
                          $forex_other=$this->db->select('to_amount')->from('forex_settings')->where(array('from_currency'=>'USD','to_currency'=>$current_currency))->get()->row();
                          echo number_format($usd_amount*$forex_other->to_amount);


                          ?>


                        </td>
                      </tr>
                         <?php }
                          ?>





                    </table>
                    
                    </td>
                    <td colspan="2" style="padding: 0;">


                      <table width="100%" class="table-bordered" border="0">
                        <tr style="font-weight: bold; border-bottom: solid 2px grey;">
                          <td>Total GBP</td>
                          <td align="right">UGX Equivalent</td>

                        </tr>


                        <?php

                        for($i='1';$i<=$c_motnth;$i++){ ?>
                          <tr>
                            <td align="right" width="40%">
                              <?php

                              $date=strtotime(date('Y-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01'));

                              $max_date=strtotime('last day of this month', $date);


                              $ra=$this->db->select_sum('received_amount')
                                  ->from('transactions')

                                  ->where(array(
                                      'created_on >='=>$date,
                                      'created_on <='=>$max_date,
                                      'recipient_currency'=>'GBP'
                                  ))
                                  ->get()->row();
                              $usd_amount=$ra->received_amount;
                              echo number_format($usd_amount);

                              ?>
                            </td>

                            <!--                        this the Uganda Equivalent-->
                            <td align="right">
                              <?php
                              $forex_other=$this->db->select('to_amount')->from('forex_settings')->where(array('from_currency'=>'USD','to_currency'=>$current_currency))->get()->row();
                              echo number_format($usd_amount*$forex_other->to_amount);


                              ?>


                            </td>
                          </tr>
                        <?php }
                        ?>





                      </table>
                    
                    </td>
                    <td colspan="2" style="padding: 0;">

                      <table width="100%" class="table-bordered" border="0">
                        <tr style="font-weight: bold; border-bottom: solid 2px grey;">
                          <td>Total EUR</td>
                          <td align="right">UGX Equivalent</td>

                        </tr>



                        <?php

                        for($i='1';$i<=$c_motnth;$i++){ ?>
                          <tr>
                            <td align="right" width="40%">
                              <?php

                              $date=strtotime(date('Y-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01'));

                              $max_date=strtotime('last day of this month', $date);


                              $ra=$this->db->select_sum('received_amount')
                                  ->from('transactions')

                                  ->where(array(
                                      'created_on >='=>$date,
                                      'created_on <='=>$max_date,
                                      'recipient_currency'=>'EUR'
                                  ))
                                  ->get()->row();
                              $usd_amount=$ra->received_amount;
                              echo number_format($usd_amount);

                              ?>
                            </td>

                            <!--                        this the Uganda Equivalent-->
                            <td align="right">
                              <?php
                              $forex_other=$this->db->select('to_amount')->from('forex_settings')->where(array('from_currency'=>'USD','to_currency'=>$current_currency))->get()->row();
                              echo number_format($usd_amount*$forex_other->to_amount);


                              ?>


                            </td>
                          </tr>
                        <?php }
                        ?>





                      </table>
                    
                    </td>
                    <td colspan="2" style="padding: 0;">

                      <table width="100%" class="table-bordered" border="0">
                        <tr style="font-weight: bold; border-bottom: solid 2px grey;">
                          <td>Total Others</td>
                          <td align="right">UGX Equivalent</td>

                        </tr>


                        <?php

                        for($i='1';$i<=$c_motnth;$i++){ ?>
                          <tr>
                            <td align="right" width="40%">
                              <?php

                              $date=strtotime(date('Y-'.str_pad($i, 2, "0", STR_PAD_LEFT).'-01'));

                              $max_date=strtotime('last day of this month', $date);


                              $ra=$this->db->query("SELECT SUM(`received_amount_usd`) AS received_amount_usd FROM (`transactions`) WHERE `created_on` >= $date AND `created_on` <= $max_date AND `recipient_currency` != 'GBP' AND `recipient_currency` != 'EUR' AND `recipient_currency` != 'USD' AND `recipient_currency` != ''")->row();
                              $usd_amount=$ra->received_amount_usd;
                              echo number_format($usd_amount);

                              ?>
                            </td>

                            <!--                        this the Uganda Equivalent-->
                            <td align="right">
                              <?php
                              $forex_other=$this->db->select('to_amount')->from('forex_settings')->where(array('from_currency'=>'USD','to_currency'=>$current_currency))->get()->row();
                              echo number_format($usd_amount*$forex_other->to_amount);


                              ?>


                            </td>
                          </tr>
                        <?php }
                        ?>





                      </table>
                    
                    </td>
                  </tr>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->