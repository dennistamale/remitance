
<?php if(isset($idii)){ ?>
<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-usd"></i>
            </div>
            <div class="details">
                <div class="number"> <?php echo $currency ?> <?php echo number_format($usd); ?> </div>
                <div class="desc"> <i class="fa fa-dollar"></i> 1  </div>
            </div>
            <a class="more" href="javascript:;"> USD
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-gbp"></i>
            </div>
            <div class="details">
                <div class="number"> <?php echo $currency ?> <?php echo number_format($gbp); ?> </div>
                <div class="desc"> <i class="fa fa-gbp"></i> 1   </div>
            </div>
            <a class="more" href="javascript:;"> GBP
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-eur"></i>
            </div>
            <div class="details">
                <div class="number"> <?php echo $currency ?> <?php echo number_format($eur); ?> </div>
                <div class="desc"> <i class="fa fa-eur"></i> 1  </div>
            </div>
            <a class="more" href="javascript:;"> EUR
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
</div>

<?php } ?>

<div class="row">
    <div class="col-md-12">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="fa fa-money font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> Currency Converter</span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('') ?>
                    <div class="form-body">
                        <div class="row">
<?php $co=$this->db->select('currency_alphabetic_code,currency_name,country')->from('country')->order_by('currency_alphabetic_code','asc')->get()->result() ?>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input has-success">
                                    <select class="form-control" name="from"  >
                                        <option value="" <?php echo set_select('from', '', TRUE); ?> >From</option>

                                        <?php foreach( $co as $from){ ?>
                                            <option value="<?php echo $from->currency_alphabetic_code  ?>"  <?php echo set_select('from', $from->currency_alphabetic_code); ?> ><?php echo $from->currency_name.' ('.$from->country.')'; ?></option>
                                        <?php } ?>

                                    </select>
                                    <label for="form_control_1">From <?php echo form_error('from','<span style=" color:red;">','</span>') ?></label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input has-success">
                                    <select class="form-control" name="to"  >
                                        <option value="" <?php echo set_select('to', '', TRUE); ?> >To</option>

                                        <?php foreach($co as $to){ ?>
                                            <option value="<?php echo $to->currency_alphabetic_code  ?>"  <?php echo set_select('to', $to->currency_alphabetic_code); ?> ><?php echo $to->currency_name.' ('.$to->country.')' ?></option>
                                        <?php } ?>

                                    </select>
                                    <label for="form_control_1">To  <?php echo form_error('to','<span style=" color:red;">','</span>') ?></label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-md-line-input has-success">
                                    <input type="text" class="form-control" name="amount" value="<?php echo set_value('amount') ?>" placeholder="Amount to Convert">
                                    <label for="form_control_1">Amount to Convert  <?php echo form_error('amount','<span style=" color:red;">','</span>') ?></label>
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>

</div>

<div class="row">


    <?php  if(isset($output2)){ ?>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat blue">
                <div class="visual">
                    <i class="fa fa-bar-chart-o"></i>
                </div>
                <div class="details">
                    <div class="number">
                        <?php

                        echo $output2.' '.$this->input->post('to');
                        ?>
                    </div>
                    <div class="desc">
                        <?php echo $amount.$this->input->post('from').'  = '.$output2.' '.$this->input->post('to');

                        ?>
                    </div>
                </div>
                <a class="more" href="#">
                    Conversion <i class="m-icon-swapright m-icon-white"></i>
                </a>
            </div>
        </div>
    <?php } ?>
</div>

