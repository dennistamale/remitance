<div class="row">

    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="fa fa-bars font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> Reason for the Hold</span>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo form_open('') ?>
                    <div class="form-body">

                        <div class="form-group">
                            <label hidden>Inline Radio</label>
                            <?php if( isset($error)){?>
                                <span class="font-red-mint" >
                                        <?php echo  $error; ?>

                                    </span>
                            <?php }  ?>
                            <?php  echo form_error('reason','<label style="color:red;">','</label>') ?>
                            <div class="radio-list">
                                <?php foreach($this->db->select()->from('hold_reasons')->get()->result() as $rs): ?>
                                <label class="radio-inlin">
                                <input type="radio" title="<?php //echo $rs->reason; ?>" name="reason" value="<?php echo $rs->title; ?>" <?php echo set_radio('reason', $rs->title); ?> />
                                    <?php echo $rs->title; ?>
                                    </label>
                                <?php endforeach ?>



                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
               <?php echo form_close() ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>

</div>