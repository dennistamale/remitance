<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-user font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> New Branch</span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
<!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">

                <?php echo form_open('',array('class'=>'form-horizontal')) ?>
                <!--                id, full_name, city, password, username, region, country, phone, email, gender, photo, user_type, sub_type, status, verified, created_on, created_by, updated_on, updated_by, id, id-->
                <div class="form-body">


                    <div class="form-group form-md-line-input">

                        <label class="col-md-2 control-label" for="form_control_1">Branch Company</label>
                        <div class="col-md-4">
                            <input type="text" required title="Enter Branch Company" value="<?php echo set_value('company') ?>" name="company" class="form-control" placeholder="Enter Branch Company" />
                            <div class="form-control-focus" style="color: red"><?php echo form_error('company') ?> </div>

                        </div>


                        <label class="col-md-2 control-label" for="form_control_1">Branch Name</label>
                        <div class="col-md-4">
                            <input type="text" required title="Enter Branch Name" value="<?php echo set_value('branch_name') ?>" name="branch_name" class="form-control" placeholder="Enter Branch Name" />
                            <div class="form-control-focus" style="color: red"><?php echo form_error('branch_name') ?> </div>

                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Telephone</label>
                        <div class="col-md-10">
                            <input type="text" required title="Enter the Telephone Number" value="<?php echo set_value('telephone') ?>" name="telephone" class="form-control" placeholder="Enter the Telephone Number" />
                            <div class="form-control-focus" style="color: red"><?php echo form_error('telephone') ?> </div>

                        </div>
                    </div>

                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Street Address</label>
                        <div class="col-md-4">
                            <input type="text" title="Enter the Street address for the Branch" value="<?php echo set_value('street_address') ?>" name="street_address" class="form-control" placeholder="Enter the Street Address" />
                            <div class="form-control-focus" style="color: red"><?php echo form_error('street_address') ?> </div>

                        </div>
                        <label class="col-md-2 control-label" for="form_control_1">Branch Code</label>
                        <div class="col-md-4">
                            <input type="text"  maxlength="2"  title="Enter the Branch Code" value="<?php echo set_value('branch_code') ?>" name="branch_code" class="form-control" placeholder="Enter the Branch Code" />
                            <div class="form-control-focus" style="color: red"><?php echo form_error('branch_code') ?> </div>

                        </div>
                    </div>


                        <div class="form-group form-md-line-input">
                            <label class="col-md-2 control-label" for="form_control_1">Country</label>
                            <div class="col-md-4">
                                <select class="form-control" name="country" id="country"  >
                                    <option value="" <?php echo set_select('country', '', TRUE); ?> >Select Country</option>

                                    <?php
                                    $s_countries=$this->db->select('country,dialing_code,a2_iso')->from('selected_countries')->get()->result();
                                    foreach($s_countries as $cat){ ?>
                                        <option value="<?php echo $cat->a2_iso  ?>"  <?php echo set_select('country', $cat->a2_iso); ?> ><?php echo $cat->country ?></option>
                                    <?php } ?>

                                </select>
                                <label for="form_control_1"> <?php echo form_error('country','<span style=" color:red;">','</span>') ?></label>
                                <span class="help-block">Choose the Country</span>
                            </div>
<!--                            this is the drop down for the city-->
                            <label class="col-md-2 control-label" for="form_control_1">City</label>
                            <div class="col-md-4">
                                <select class="form-control" name="city" id="city" >
                                    <option value="" <?php echo set_select('city', '', TRUE); ?> >Select Branch City</option>



                                </select>
                                <label for="form_control_1"> <?php echo form_error('city','<span style=" color:red;">','</span>') ?></label>
                                <span class="help-block">Choose the City for branch</span>
                            </div>


                        </div>


                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-10">
                                <button type="reset" class="btn default"> <i class="fa fa-remove"></i> Cancel</button>
                                <button type="submit" class="btn green-jungle"><i class="fa fa-save"></i> Save</button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->

        </div>


    </div>

    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

<?php $this->load->view($this->page_level.$this->page_level2.'select_state'); ?>
