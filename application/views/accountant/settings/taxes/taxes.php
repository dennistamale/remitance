<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-users font-dark"></i>
                    <span class="caption-subject bold uppercase">Taxes &nbsp;</span>
                </div>
                <div class="actions pull-left">

            <?php echo anchor($this->page_level.$this->page_level2.'add_taxes',' <i class="fa fa-plus"></i> Add Taxes','class="btn green-jungle btn-sm"'); ?>
                </div>
                <div class="tools"> </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th width="5">#</th>
                        <th> Country </th>
                        <th> Name </th>
                        <th> Type </th>
                        <th> %age </th>
                        <th>Taxed</th>
                        <th width="70">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $no=1;
                    foreach($this->db->select('a.*,b.country')->from('taxes a')->join('selected_countries b','a.country=b.a2_iso')->order_by('a.country','asc')->get()->result() as $c): ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td> <?php echo $c->country ?> </td>
                            <td> <?php echo $c->name ?> </td>
                            <td> <?php echo humanize($c->type) ?> </td>
                            <td><?php echo $c->unit_value ?></td>
                            <td> <?php echo humanize($c->taxed); ?> </td>

                            <td>
                                <div class="btn-group">
                                    <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                        <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li>

                                            <?php echo anchor($this->page_level.$this->page_level2.'delete_taxes/'.$c->id*date('Y'),'  <i class="fa fa-trash-o"></i> Delete','onclick="return confirm(\'Are you sure you want to delete ?\')"') ?>
                                        </li>
                                        <li>
                        <?php echo anchor($this->page_level.$this->page_level2.'edit_taxes/'.$c->id*date('Y'),'  <i class="fa fa-edit"></i> Edit'); ?>
                                        </li>

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php
                    $no++;
                    endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->