


<!--this is the part for nested portlets-->
<div class="row" >
    <div class="col-md-12" style="margin-bottom: -20px;">

        <?php //echo  $this->site_options->title('send_to_branch_without_money'); ?>

        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light bordered">

            <div class="portlet-body">




                <div class="row">
                    <div class="col-md-5 pull-right" >
                        <!-- BEGIN PORTLET -->
                        <div class="portlet light  tasks-widget">
                            <div class="portlet-title">
                                <div class="caption caption-md">
                                    <i class="icon-bar-chart theme-font hide"></i>
                                    <span class="caption-subject font-blue-madison bold uppercase">Search Users</span>
                                    <span class="caption-helper"></span>
                                </div>

                                <div class="inputs">
                                    <div class="portlet-input input-small input-inline">

                                        <?php echo form_open($this->page_level.$this->page_level2.'search_users','class="search-form"') ?>
                                        <div class="input-group">
                                            <input type="text" class="form-control input-sm" required placeholder="Search..." name="query" value="<?php echo set_value('query') ?>" >
                            <span class="input-group-btn">
                                <button type="submit" style="background: none;margin:-50px" href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier btn-sm btn-success"></i>
                                </button>
                            </span>
                                        </div>
                                        <?php echo form_close() ?>

                                    </div>
                                </div>

                            </div>
                            <div class="portlet-body">
                                <div class="task-content">
                                    <div class="scroller" style="height: 162px;" data-always-visible="1" data-rail-visible1="0" data-handle-color="#D7DCE2">
                                        <!-- START TASK LIST -->
                                        <ul class="task-list">
                                            <?php if(isset($search_results[0]->id)){ ?>

                                                <?php $no=1; foreach($search_results as $sc): ?>
                                                    <li>

                                                        <div class="task-title"><i class="badge badge-default"><?php echo $no; ?></i>

                                                            <?php $city=$this->db->select('title')->from('state')->where('state',$sc->city)->get()->row(); ?>
                                                            <?php echo anchor($this->page_level.$this->page_level2.'new/'.$sc->id*date('Y'),' <span class="task-title-sp"> <b>'.$sc->full_name.'</b> '.(isset($city->title)?$city->title:$sc->city).' ('.$sc->phone.')  </span>  <span class="label label-sm label-success pull-right">Select</span>','title="Select user you want to send Money" style="text-decoration:none;"') ?>



                                                        </div>

                                                    </li>
                                                    <?php $no++; endforeach; ?>
                                                <?php
                                            }
                                            else
                                            {
                                                ?>
                                                <li>

                                                    <div class="task-title">
                                                        <span class="task-title-sp"> <b>No results found</b></span>
                                                        <span class="label label-sm label-success pull-right">Select</span>


                                                    </div>

                                                </li>
                                            <?php } ?>




                                        </ul>
                                        <!-- END START TASK LIST -->
                                    </div>
                                </div>
                                <div class="task-footer">
                                    <div class="btn-arrow-link pull-right">

                                        <?php echo anchor($this->page_level.'users','View All Users') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END PORTLET -->


                    </div>
                    <div class="col-md-7 pull-left" style="border-right: solid thin lightgrey">
                        <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption font-green-haze">
                                    <i class="icon-directions font-green-haze"></i>
                                    <span class="caption-subject bold uppercase"> Senders Details</span>
                                </div>

                                <div class="actions">

                                    <?php echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Transactions','class="btn btn-circle btn-warning btn-sm"'); ?>
                                </div>
                            </div>
                            <div class="portlet-body form">

                                <?php echo form_open('',array('class'=>'form-horizontal')) ?>

                                <!--                This is the script which gets the users and ther related data-->

                                <?php isset($id)?$this->load->view($this->page_level.$this->page_level2.'auto_load'):$this->load->view($this->page_level.$this->page_level2.'auto_load_empty') ?>

                            </div>
                            <!-- END SAMPLE FORM PORTLET-->

                        </div>



                    </div>


                </div>




            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
</div>
<!--this is the end of the part for nestetep portlets-->

<!--this is the part for nested portlets-->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN Portlet PORTLET-->
        <div class="portlet light bordered">

            <div class="portlet-body">




                <div class="row">
                    <div class="col-md-7" >
                        <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption font-green-haze">
                                    <i class="icon-directions font-green-haze"></i>
                                    <span class="caption-subject bold uppercase"> Recipient's Details </span>
                                </div>

                                <div class="actions">


                                </div>
                            </div>
                            <div class="portlet-body form">

                                <div class="form-body">


                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-4 control-label" for="form_control_1">Receiver Name</label>
                                        <div class="col-md-8">
                                            <input type="text" required title="Enter the Full Names of the Receiver" value="<?php echo set_value('receiver_name') ?>" name="receiver_name" class="form-control" placeholder="Enter the Full Names of the Receiver" />
                                            <div class="form-control-focus" style="color: red"><?php echo form_error('receiver_name') ?> </div>

                                        </div>
                                    </div>


                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-4 control-label" for="form_control_1">Receiver's Country / Street  <span class="text text-danger">*</span></label>
                                        <div class="col-md-4">
                                            <select class="form-control" name="country" id="cntry" required >
                                                <option value="" <?php echo set_select('country', '', TRUE); ?> >Select Country</option>

                                                <?php foreach($this->db->select('country,dialing_code,a2_iso')->from('selected_countries')->get()->result() as $cat){ ?>
                                                    <option value="<?php echo $cat->a2_iso  ?>"  <?php echo set_select('country', $cat->a2_iso); ?> ><?php echo $cat->country ?></option>
                                                <?php } ?>

                                            </select>
                                            <label for="form_control_1"> <?php echo form_error('country','<span style=" color:red;">','</span>') ?></label>
                                            <span class="help-block">Choose the Country</span>

                                        </div>

<!--                                        this is the selection for the branch-->

                                        <div class="col-md-4" >

                                            <select class="form-control" name="branch" id="branch"  required  >
                                                <option value="" <?php echo set_select('branch', '', TRUE); ?> >Select branch</option>

                                                <?php echo form_error('branch','<span style=" color:red;">','</span>') ?>


                                            </select>

                                        </div>
<!--This is the end of the selection for the branches-->

                                    </div>


                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-4 control-label" for="form_control_1">Receiver Email</label>
                                        <div class="col-md-8">
                                            <input type="email" class="form-control"  name="receiver_email" value="<?php echo set_value('receiver_email') ?>" id="form_control_1" placeholder="Receiver Email address">
                                            <div class="form-control-focus" style="color: red"><?php echo form_error('receiver_email') ?> </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                    <br/>
                                    <div class="form-group form-md-line-input">
                                        <label class="col-md-4 control-label" for="form_control_1">Zip Code / Phone Number</label>
                                        <div class="col-md-3">
                                            <select class="form-control" name="receiver_zip_code"  >
                                                <option value="" <?php echo set_select('receiver_zip_code', '', TRUE); ?> >Zip Code</option>

                                                <?php foreach($this->db->select('dialing_code,a2_iso')->from('country')->order_by('a2_iso','asc')->get()->result() as $cat){ ?>
                                                    <option value="<?php echo $cat->dialing_code  ?>"  <?php echo set_select('zip_code', $cat->dialing_code); ?> ><?php echo $cat->a2_iso.'('.$cat->dialing_code.')' ?></option>
                                                <?php } ?>

                                            </select>
                                            <label for="form_control_1"> <?php echo form_error('receiver_zip_code','<span style=" color:red;">','</span>') ?></label>
                                            <span class="help-block">Choose the Zip code for your country</span>
                                        </div>
                                        <div class="col-md-5">
                                            <input maxlength="10" type="text" class="form-control" required name="receiver_phone" value="<?php echo set_value('receiver_phone') ?>" id="form_control_1" placeholder="07xxxxxx">
                                            <div class="form-control-focus" style="color: red"><?php echo form_error('receiver_phone') ?> </div>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>




                                </div>
                            </div>


                            <!-- END SAMPLE FORM PORTLET-->

                        </div>



                    </div>
                    <div class="col-md-5" style="border-left: solid thin lightgrey">
                        <!-- BEGIN SAMPLE FORM PORTLET-->
                        <div class="portlet light">
                            <div class="portlet-title">
                                <div class="caption font-red-sunglo">
                                    <span class="caption-subject bold uppercase">Transaction Details</span>
                                </div>

                            </div>
                            <div class="portlet-body form">
                                <div class="row" id="commission_rates">

                                <?php //$this->load->view($this->page_level.$this->page_level2.'commission_rates') ?>

                                </div>

<!--                            <iframe src="--><?php //echo base_url('index.php/admin/portlet') ?><!--" scrolling="no" width="100%" style="border: none"></iframe>-->


                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group  has-success form-md-floating-label">Sender's Currency <span class="text text-danger">*</span>
                                                <select class="form-control" id="sender_currency" name="currency"  required >
                                                    <option value="" <?php echo set_select('currency', '', TRUE); ?> >Select Currency</option>

                                                    <?php foreach($this->db->select('distinct(from_currency)')->from('forex_settings')->get()->result() as $cat){ ?>
                                                        <option value="<?php echo $cat->from_currency  ?>"  <?php echo set_select('currency', $cat->from_currency); ?> ><?php echo $cat->from_currency ?></option>
                                                    <?php } ?>

                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group  has-success form-md-floating-label">Recipient Currency<span class="text text-danger">*</span>
                                                <select class="form-control" name="convert_currency"  required >
                                                    <option value="" <?php echo set_select('convert_currency', '', TRUE); ?> >Select Currency</option>

                                                    <?php foreach($this->db->select('distinct(to_currency)')->from('forex_settings')->get()->result() as $cat){ ?>
                                                        <option value="<?php echo $cat->to_currency  ?>"  <?php echo set_select('convert_currency', $cat->to_currency); ?> ><?php echo $cat->to_currency ?></option>
                                                    <?php } ?>

                                                </select>
                                            </div>
                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group  has-success form-md-floating-label">Gross Amount<span class="text text-danger">*</span>  <div id="alert"></div>
                <input id="amount" name="amount"  type="text"  step="any" placeholder="Gross Amount" value="<?php echo set_value('amount') ?>"  required class="form-control">


                                                <div class="form-control-focus" style="color: red"><?php echo form_error('amount') ?> </div>
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            <div class="form-group  has-success form-md-floating-label">Commission (USD)
                                                <input readonly style="border:none; font-weight: bold" id="computed_commission"  type="text" placeholder="Commission Computed" value="" name="commission_computed" class="form-control">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">



                                        <div class="col-md-6">
                                            <div class="form-group  has-success form-md-floating-label">Taxes
                                                <input  readonly  placeholder="Taxes" style="border:none; font-weight: bold;" id="other_charges" type="text"  value="0" name="other_charges" class="form-control">
                                            </div>
                                        </div>




                                        <div class="col-md-6">
                                            <span class="text text-danger" id="netamount_alert"></span>
                                            <div class="form-group  has-success form-md-floating-label">Net Amount<span class="text text-danger">*</span>
                                                <input style=" font-weight: bold" id="net_amount"  placeholder="Net Amount" type="text" step="any" value="" name="net_amount" class="form-control">
                                            </div>
                                        </div>



                                    </div>



<!--                                    these computationa are made internaly-->
                                    <div class="row">


                                        <div class="col-md-12 hidden">
                                            <div class="note note-info"><span class="text text-primary">For Reverse Transfer Please Add Percentage rate for ranges from the List</span>

                                                <span class="text text-danger" id="com_alert"></span>

                                            <div class="form-group  has-success form-md-floating-label">Commission<span class="text text-danger">(Optional)</span>
                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i>%</i>
                                                                    </span>
                                                    <input name="commission" id="commission"   type="number" step="any" max="100"  class="form-control">


                                                </div>
                                                <div class="form-control-focus" style="color: red"><?php echo form_error('commission') ?> </div>
                                            </div>




                                        </div>
                                        </div>



                                    </div>


                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <button type="reset" class="btn default">Cancel</button>
                                </div>
                                <?php echo form_close(); ?>
                            </div>
                        </div>
                        <!-- END SAMPLE FORM PORTLET-->



                    </div>
                </div>



            </div>
        </div>
        <!-- END Portlet PORTLET-->
    </div>
</div>
<!--this is the end of the part for nestetep portlets-->




<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>



<?php  $this->load->view($this->page_level.$this->page_level2.'country_branches'); ?>
<?php  $this->load->view($this->page_level.$this->page_level2.'commission_ajax'); ?>

