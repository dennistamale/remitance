<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark caption-subject font-green bold uppercase">

                    <?php echo form_open($this->page_level.$this->page_level2.$this->uri->slash_segment(3),'class="form-inline"') ?>
                    <?php  echo isset($first_day)? humanize($subtitle).' Report From : '.date('d M Y',$first_day).' - '.date('d M Y',$last_day):humanize($subtitle); ?>
                    <div class="form-group input-group-sm input-medium  hidden-print">

                        <div class="input-group date-picker input-daterange" data-date="<?php echo date('Y-m-d') ?>" data-date-format="yyyy-mm-dd">
                            <span class="input-group-addon">From </span>
                            <input type="text" class="form-control" name="from" value="<?php echo set_value('from') ?>">
                            <span class="input-group-addon">to </span>
                            <input type="text" class="form-control" name="to" value="<?php echo set_value('to')?>">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-sm green"><i class="fa fa-sliders"></i> Filter</button>
                    <?php echo form_close(); ?>
                </div>
                <div class="tools">
<!--                    <div class="btn-group  hidden-print">-->
<!--                        <button class="btn blue  btn-outline " data-toggle="dropdown"  onclick="javascript:window.print();"><i class="fa fa-print"></i> Print </button>-->
<!--                    </div>-->
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <!--id, sender_id, sent_amount, commission, received_amount, secret_code, sender_country, receiver_country, receiver_street_address, receiver_name, receiver_email, receiver_phone, receiver_gender, transfer_type, branch_id, created_on, created_by, updated_on, updated_by, status, -->
                    <thead>
                    <tr>
                        <th> Sender </th>
                        <th> Recipient </th>


                        <th>Received </th>

                        <th> Phone </th>

                        <th>Sender Country</th>
                        <th> Branch </th>
                        <th>Date</th>
                        <th> Status </th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php  foreach($t as $trans): ?>
                        <tr>
                            <td title="View Sender Profile Details">


                                <?php
                                $user=$this->db->select('full_name')->from('users')->where('id',$trans->sender_id)->get()->row();
                                echo anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'), isset($user->full_name)?ucwords($user->full_name):'N/A') ?>
                                </span>
                                <span class="visible-print"><?php echo isset($user->full_name)?ucwords($user->full_name):'N/A' ?></span>

                            </td>
                            <td > <?php echo isset($trans->receiver_name)?ucwords($trans->receiver_name):'N/A'; ?> </td>


                            <td align="right"> <?php echo number_format($trans->received_amount) ?> </td>

                            <td>  <?php echo $trans->receiver_phone ?> </td>

                            <td style="white-space: nowrap;">  <?php
                                $c=$this->db->select('country')->from('country')->where('a2_iso',$trans->sender_country)->get()->row();
                                echo isset($c->country)?character_limiter($c->country,20):'N/A'; ?>
                            </td>

                            <td>  <?php

                                $branch= $this->db->select('branch_name')->from('branch')->where('id',$trans->branch_id)->get()->row();

                                echo count($branch)?$branch->branch_name: $trans->branch_id; ?> </td>
                            <td style="white-space: nowrap;"><?php echo date('d-m-Y',$trans->created_on) ?></td>
                            <td>

                                <?php if($trans->status=='cashed_out'){ ?>
                                    <div class="btn green-jungle btn-sm" style="width:100px;"><?php echo humanize($trans->status) ?></div>
                                <?php }else{ ?>

                                    <div class="btn btn-info btn-sm" style="width:100px;"><?php echo humanize($trans->status) ?></div>

                                <?php } ?>
                            </td>


                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->


<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->