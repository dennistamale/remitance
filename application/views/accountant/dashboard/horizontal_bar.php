<?php //echo base_url() ?><!--assets/horizontal_bar/-->


<link href="<?php echo base_url() ?>assets/horizontal_bar/build/css/style.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/horizontal_bar/build/css/horizBarChart.css" media="all" rel="stylesheet" type="text/css" />

<!---->
<!--<link href="build/css/style.css" media="all" rel="stylesheet" type="text/css" />-->
<!--<link href="build/css/horizBarChart.css" media="all" rel="stylesheet" type="text/css" />-->


<div class="row">
    <div class="col-md-6">
<div class="wrapper">
    <section>

        <!-- Code Start -->
        <div class="chart-horiz">
            <!-- Actual bar chart -->
            <ul class="chart" style="width:70%; overflow: visible;">
                <li class="title" title="TITLE 1"></li>
                <li class="current" title="Label 1"><span class="bar" data-number="38000"></span><span class="number">38,000</span></li>
                <li class="current" title="Label 2"><span class="bar" data-number="28500"></span><span class="number">28,500</span></li>
                <li class="current" title="Label 3"><span class="bar" data-number="128000"></span><span class="number">128,000</span></li>
                <li class="current" title="Label 4"><span class="bar" data-number="134000"></span><span class="number">134,000</span></li>
            </ul>
        </div>
        <!-- Code End -->

    </section>

</div>
    </div>
</div>