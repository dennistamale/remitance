<div class="row">

    <div class="col-md-12 ">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <i class="fa fa-edit font-red-sunglo"></i>
                    <span class="caption-subject bold uppercase"> Reason for the cancellation </span>
                </div>

            </div>
            <div class="portlet-body form">
                <?php echo form_open('') ?>
                    <div class="form-body">

                        <div class="form-group">
                            <?php echo form_error('reason','<label style="color:red;">','</label>'); ?>
                           <textarea name="reason" required placeholder="Reason why cancelling the transaction" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn blue">Submit</button>
                        <button type="button" class="btn default">Cancel</button>
               <?php echo form_close() ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>

</div>