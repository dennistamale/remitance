
<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">

                <div class="caption font-dark hidden-print">



                        <?php echo form_open($this->page_level.$this->page_level2.'filter','class="form-inline"') ?>

                    <div class="form-group" style="margin-bottom: 0;">
                        <?php  echo anchor($this->page_level.$this->page_level2.'new',' <i class="icon-paper-plane"></i> New Transfer','class="btn green-jungle btn-outline tooltips" data-original-title="Click For New Transfers" '); ?>



                        <?php echo anchor($this->page_level.'import/transactions','<i class="fa fa-file-excel-o"></i> Import.XLS','class="btn green  btn-outline"') ?>





                        <div class="input-group" >
                            <span class="input-group-addon font-green">ENTER TXN</span>
                            <input type="search" class="form-control" name="transaction" placeholder="Transaction ID" value="<?php echo set_value('transaction') ?>">

                        </div>


<!--                        <div class="input-group input-medium date-picker input-daterange" data-date="--><?php //echo date('Y-m-d') ?><!--" data-date-format="yyyy-mm-dd">-->
<!--                            <span class="input-group-addon">From </span>-->
<!--                            <input type="text" class="form-control" name="from" value="--><?php //echo set_value('from') ?><!--">-->
<!--                            <span class="input-group-addon">to </span>-->
<!--                            <input type="text" class="form-control" name="to" value="--><?php //echo set_value('to')?><!--">-->
<!--                        </div>-->
<!---->
<!--                        --><?php //echo form_error('country','<label style="color:#ff0000;">','</label>'); ?>
<!--                        <select class="form-control" name="country" style="width: 110px;">-->
<!--                            <option value="" --><?php //echo set_select('country', '', TRUE); ?><!-- >Country</option>-->
<!--                            --><?php //foreach($this->db->select('a2_iso,country')->from('selected_countries')->get()->result() as $cty): ?>
<!--                                <option value="--><?php //echo $cty->a2_iso ?><!--" --><?php //echo set_select('country', $cty->a2_iso); ?><!-- >--><?php //echo $cty->country ?><!--</option>-->
<!--                            --><?php //endforeach; ?>
<!---->
<!---->
<!---->
<!--                        </select>-->
<!---->
<!--                        --><?php //echo form_error('status','<label style="color:#ff0000;">','</label>'); ?>
<!---->
<!--                        <select class="form-control" name="status" style="width: 100px;" >-->
<!--                            <option value="" --><?php //echo set_select('status', '', TRUE); ?><!-- >Status</option>-->
<!--                            --><?php //foreach($status as $st){ ?>
<!--                                --><?php //if( $st!='pending'){ ?>
<!--                                    <option value="--><?php //echo $st ?><!--" --><?php //echo set_select('status', $st); ?><!-- >--><?php //echo humanize($st) ?><!--</option>-->
<!---->
<!--                                --><?php //} } ?>
<!---->
<!---->
<!--                        </select>-->

                        <button type="submit" class="btn btn-outline blue"><i class="fa fa-sliders"></i> Filter</button>

                    </div>

                        <?php echo form_close(); ?>



                </div>

            </div>
            <div class="portlet-body">

                <?php if(isset($t)) { ?>

                <table class="table table-striped table-bordered table-hover" id="sample_1">
                    <!--id, sender_id, sent_amount, commission, received_amount, secret_code, sender_country, receiver_country, receiver_street_address, receiver_name, receiver_email, receiver_phone, receiver_gender, transfer_type, branch_id, created_on, created_by, updated_on, updated_by, status, -->
                    <thead>
                    <tr>
                        <th hidden></th>
                        <th>Trans ID</th>
                        <th> Sender </th>
                        <th> Recipient </th>
                        <th title="Amount Paid"> Net Amt</th>
                        <th>Source</th>
                        <th>Destination</th>
                        <th> Branch </th>
                        <th width="90">Date Sent</th>
                        <th> Status </th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php


                        foreach ($t as $trans): ?>
                            <tr>
                                <td hidden></td>
                                <td><?php
                                    echo anchor($this->page_level . 'cashout/process/' . $trans->id * date('Y'), (strlen($trans->transaction_id) ? $trans->transaction_id : $trans->secret_code), 'style="text-decoration:none;font-weight:bold;"')
                                    ?></td>
                                <td title="Click to see Senders Details">
                                    <?php
                                    $user = $this->db->select('full_name')->from('users')->where('id', $trans->sender_id)->get()->row();
                                    echo isset($user->full_name) ? anchor($this->page_level . 'users/edit/' . $trans->sender_id * date('Y'), (ucwords(word_limiter($user->full_name, 2)))) : 'N/A'; ?>

                                </td>
                                <td title="Click to see Recievers Transaction Details"> <?php
                                    echo anchor($this->page_level . 'cashout/process/' . $trans->id * date('Y'), ucwords(word_limiter($trans->receiver_name, 2)));
                                    ?> </td>


                                <td align="right" title="Amount Paid"><span
                                        style="font-size: x-small"><?php echo $trans->recipient_currency ?></span><?php echo number_format($trans->received_amount, 2) ?>
                                </td>

                                <td><?php
                                    $d = $this->db->select('country')->from('country')->where('a2_iso', $trans->sender_country)->get()->row();
                                    echo isset($d->country) ? word_limiter($d->country, 2) : 'N/A'; ?></td>

                                <td>  <?php
                                    $c = $this->db->select('country')->from('country')->where('a2_iso', $trans->receiver_country)->get()->row();
                                    echo isset($c->country) ? word_limiter($c->country, 2) : 'N/A'; ?>
                                </td>

                                <td>  <?php
                                    $bra = $this->db->select('id,branch_name')->from('branch')->where('id', $trans->branch_id)->get()->row();
                                    echo isset($bra->id) ? word_limiter($bra->branch_name, 3) : 'N/A'; ?> </td>

                                <td><?php echo date('d-m-Y', $trans->created_on) ?></td>
                                <td>

                                    <?php if ($trans->status == 'cashed_out') { ?>


                                        <div class="btn-group">
                                            <a class="btn green-jungle btn-sm" style="width:130px;" href="javascript:;"
                                               data-toggle="dropdown">
                                                <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i
                                                    class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <?php if (($trans->created_by != $this->session->userdata('id'))) { ?>
                                                    <li>
                                                        <?php echo ($trans->status != 'not_approved') ? anchor($this->page_level . 'cashout/process/' . $trans->id * date('Y'), '  <i class="fa fa-money"></i> Cashout') : '' ?>
                                                    </li>
                                                <?php } ?>

                                                <li>
                                                    <?php echo anchor($this->page_level . 'cashout/process/' . $trans->id * date('Y'), '  <i class="fa fa-money"></i> Transaction Details') ?>
                                                </li>
                                                <li>
                                                    <?php
                                                    echo anchor($this->page_level . 'users/edit/' . $trans->sender_id * date('Y'), '<i class="fa fa-user"></i>View Profile');
                                                    ?>
                                                </li>


                                            </ul>
                                        </div>
                                    <?php } elseif ($trans->status == 'not_approved') { ?>


                                        <div class="btn-group">
                                            <a class="btn default btn-sm" style="width:130px;" href="javascript:;"
                                               data-toggle="dropdown">
                                                <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i
                                                    class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">
                                                <?php if (($trans->created_by != $this->session->userdata('id'))) { ?>
                                                    <li>
                                                        <?php echo ($trans->status != 'not_approved') ? anchor($this->page_level . 'cashout/process/' . $trans->id * date('Y'), '  <i class="fa fa-money"></i> Cashout') : '' ?>
                                                    </li>
                                                <?php } ?>

                                                <li>
                                                    <?php echo anchor($this->page_level . 'cashout/process/' . $trans->id * date('Y'), '  <i class="fa fa-money"></i> Transaction Details') ?>
                                                </li>
                                                <li>
                                                    <?php
                                                    echo anchor($this->page_level . 'users/edit/' . $trans->sender_id * date('Y'), '<i class="fa fa-user"></i>View Profile');
                                                    ?>
                                                </li>
                                                <li>

                                                    <?php echo $trans->status == 'hold' ? anchor($this->page_level . $this->page_level2 . 'remove_hold/' . $trans->id * date('Y'), '  <i class="icon-control-play"></i> Remove Onhold') : anchor($this->page_level . $this->page_level2 . 'hold/' . $trans->id * date('Y'), '  <i class="icon-control-pause"></i> Put Onhold') ?>
                                                </li>
                                                <li>
                                                    <?php echo anchor($this->page_level . 'transfer/cancel/' . $trans->id * date('Y'), '<i class="fa fa-ban"></i> Cancel Transaction') ?>
                                                </li>


                                            </ul>
                                        </div>
                                    <?php } elseif ($trans->status == 'hold') { ?>


                                        <div class="btn-group">
                                            <a class="btn dark btn-sm tooltips"
                                               data-original-title="<?php echo $trans->hold_reason ?>"
                                               style="width:130px;" href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i
                                                    class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">


                                                <li>
                                                    <?php echo anchor($this->page_level . 'cashout/process/' . $trans->id * date('Y'), '  <i class="fa fa-money"></i> Transaction Details') ?>
                                                </li>
                                                <li>
                                                    <?php
                                                    echo anchor($this->page_level . 'users/edit/' . $trans->sender_id * date('Y'), '<i class="fa fa-user"></i>View Profile');
                                                    ?>
                                                </li>
                                                <li>

                                                    <?php echo $trans->status == 'hold' ? anchor($this->page_level . $this->page_level2 . 'remove_hold/' . $trans->id * date('Y'), '  <i class="icon-control-play"></i> Remove Onhold') : anchor($this->page_level . $this->page_level2 . 'hold/' . $trans->id * date('Y'), '  <i class="icon-control-pause"></i> Put Onhold') ?>
                                                </li>
                                                <li>
                                                    <?php echo anchor($this->page_level . 'transfer/cancel/' . $trans->id * date('Y'), '<i class="fa fa-ban"></i> Cancel Transaction') ?>
                                                </li>


                                            </ul>
                                        </div>
                                    <?php } elseif ($trans->status == 'canceled') { ?>

                                        <div class="btn-group">
                                            <a class="btn red btn-sm tooltips" style="width:130px;"
                                               data-original-title="<?php echo $trans->other_reason ?>"
                                               href="javascript:;" data-toggle="dropdown">
                                                <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i
                                                    class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="dropdown-menu pull-right">


                                                <li>
                                                    <?php echo anchor($this->page_level . 'cashout/process/' . $trans->id * date('Y'), '  <i class="fa fa-money"></i> Transaction Details') ?>
                                                </li>
                                                <li>
                                                    <?php
                                                    echo anchor($this->page_level . 'users/edit/' . $trans->sender_id * date('Y'), '<i class="fa fa-user"></i>View Profile');
                                                    ?>
                                                </li>


                                            </ul>
                                        </div>

                                    <?php } else { ?>


                                        <?php if ($trans->created_by == $this->session->userdata('id')) { ?>


                                            <div class="btn-group">
                                                <a class="btn yellow-gold btn-sm" style="width:130px;"
                                                   href="javascript:;" data-toggle="dropdown">
                                                    <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i
                                                        class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <?php if (($trans->created_by != $this->session->userdata('id'))) { ?>
                                                        <li>
                                                            <?php echo ($trans->status != 'not_approved') ? anchor($this->page_level . 'cashout/process/' . $trans->id * date('Y'), '  <i class="fa fa-money"></i> Cashout') : '' ?>
                                                        </li>
                                                    <?php } ?>

                                                    <li>
                                                        <?php echo anchor($this->page_level . 'cashout/process/' . $trans->id * date('Y'), '  <i class="fa fa-money"></i> Transaction Details') ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                        echo anchor($this->page_level . 'users/edit/' . $trans->sender_id * date('Y'), '<i class="fa fa-user"></i>View Profile');
                                                        ?>
                                                    </li>
                                                    <li>

                                                        <?php echo $trans->status == 'hold' ? anchor($this->page_level . $this->page_level2 . 'remove_hold/' . $trans->id * date('Y'), '  <i class="icon-control-play"></i> Remove Onhold') : anchor($this->page_level . $this->page_level2 . 'hold/' . $trans->id * date('Y'), '  <i class="icon-control-pause"></i> Put Onhold') ?>
                                                    </li>
                                                    <li>
                                                        <?php echo anchor($this->page_level . 'transfer/cancel/' . $trans->id * date('Y'), '<i class="fa fa-ban"></i> Cancel Transaction') ?>
                                                    </li>


                                                </ul>
                                            </div>

                                        <?php } else { ?>


                                            <div class="btn-group">
                                                <a class="btn btn-primary btn-sm" style="width:130px;"
                                                   href="javascript:;" data-toggle="dropdown">
                                                    <i class="fa fa-cogs"></i> <?php echo humanize($trans->status) ?> <i
                                                        class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right">
                                                    <?php if (($trans->created_by != $this->session->userdata('id'))) { ?>
                                                        <li>
                                                            <?php echo ($trans->status != 'not_approved') ? anchor($this->page_level . 'cashout/process/' . $trans->id * date('Y'), '  <i class="fa fa-money"></i> Cashout') : '' ?>
                                                        </li>
                                                    <?php } ?>

                                                    <li>
                                                        <?php echo anchor($this->page_level . 'cashout/process/' . $trans->id * date('Y'), '  <i class="fa fa-money"></i> Transaction Details') ?>
                                                    </li>
                                                    <li>
                                                        <?php
                                                        echo anchor($this->page_level . 'users/edit/' . $trans->sender_id * date('Y'), '<i class="fa fa-user"></i>View Profile');
                                                        ?>
                                                    </li>
                                                    <li>

                                                        <?php echo $trans->status == 'hold' ? anchor($this->page_level . $this->page_level2 . 'remove_hold/' . $trans->id * date('Y'), '  <i class="icon-control-play"></i> Remove Onhold') : anchor($this->page_level . $this->page_level2 . 'hold/' . $trans->id * date('Y'), '  <i class="icon-control-pause"></i> Put Onhold') ?>
                                                    </li>
                                                    <li>
                                                        <?php echo anchor($this->page_level . 'transfer/cancel/' . $trans->id * date('Y'), '<i class="fa fa-ban"></i> Cancel Transaction') ?>
                                                    </li>


                                                </ul>
                                            </div>


                                        <?php } ?>

                                    <?php } ?>
                                </td>


                            </tr>
                        <?php endforeach;



                    ?>

                    </tbody>
                </table>
                <?php }else{

                   $data= array(
                       'alert'=>'info',
                       'message'=>'Enter Transaction ID to get the details',
                       'hide'=>1
                   );

                    $this->load->view('alert',$data);

                } ?>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->


<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->