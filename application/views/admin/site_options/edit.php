<?php  ?>


<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/global/plugins/jquery-minicolors/jquery.minicolors.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->


<div class="row">
    <div class="col-md-12">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="fa fa-edit font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> Edit <?php echo humanize($op->option_name)  ?></span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open_multipart('','class="form-horizontal"') ?>

                <div class="form-body">

                    <div class="row">


                        <div class="form-group">
                            <label class="col-md-3 control-label">Value</label>
                            <div class="col-md-9">

                                <?php if($op->option_name=='site_color_code'){

//                                    echo '<span style="color:#fff; background-color:'.$op->option_value.';">'.$op->option_value.'</span>'; ?>
                                   
                                    <input type="text" id="hue-demo"  data-control="hue"  name="option_value"  value="<?php echo $op->option_value ?>" class="form-control demo" placeholder="Enter Value">
                              <?php  }elseif($op->option_name=='site_logo'){ ?>

                                    <div class="form-group">

                                        <?php if( isset($error)){?>
                                            <span class="font-red-mint" >
                                        <?php echo  $error; ?>

                                    </span>
                                        <?php } echo form_error('image','<label style="color:red;">','</label>') ?>

                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                <img src="<?php echo strlen($op->option_value)>0?base_url().$op->option_value:base_url().'assets/assets/img/avatars/placeholder.png' ?>" alt=""/>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                                            </div>
                                            <div>
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input type="file" name="image" required>
																</span>
                                                <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                                    Remove </a>
                                            </div>
                                        </div>

                                    </div>

                                    <?php

                                } else{

                                 ?>


                                <input type="text" name="option_value"  value="<?php echo $op->option_value ?>" class="form-control" placeholder="Enter Value">

                                <?php } ?>

                                <?php echo form_error('option_value','<span style=" color:red;">','</span>') ?>
<!--                                <span class="help-block"> A block of help text. </span>-->

                            </div>
                        </div>

                    </div>

                </div>
                <div class="form-actions">
                    <button type="submit" class="btn blue">Submit</button>
                    <button type="button" class="btn default">Cancel</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>

</div>


<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>


