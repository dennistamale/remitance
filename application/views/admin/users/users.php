<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">


                <div class="caption font-dark hidden-print">



                </div>
                <div class="tools">

                    <!--this is the begining of the filters-->
                    <?php echo form_open($this->page_level.$this->page_level2.'filter','class="form-inline"') ?>
                    <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn green-jungle"'); ?>

                    <div class="form-group">
                        <?php echo form_error('country','<label style="color:#ff0000;">','</label>'); ?>
                        <select class="form-control" name="country" style="width: 110px;">
                            <option value="" <?php echo set_select('country', '', TRUE); ?> >Country</option>
                            <?php foreach($this->db->select('a2_iso,country')->from('selected_countries')->get()->result() as $cty): ?>
                                <option value="<?php echo $cty->a2_iso ?>" <?php echo set_select('country', $cty->a2_iso); ?> ><?php echo $cty->country ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <?php if($this->session->userdata('user_type')!='2'){ ?>

                        <div class="form-group">
                            <?php echo form_error('role','<label style="color:#ff0000;">','</label>'); ?>
                            <select class="form-control" name="role" style="width: 100px;" >
                                <option value="" <?php echo set_select('role', '', TRUE); ?> >Role</option>
                                <?php foreach($this->db->select('id,title')->from('user_type')->get()->result() as $role): ?>
                                    <option value="<?php echo $role->id  ?>" <?php echo set_select('role', $role->id); ?> ><?php echo humanize($role->title) ?></option>
                                <?php endforeach; ?>



                            </select>
                        </div>

                    <?php } ?>

                    <button type="submit" class="btn btn-primary"><i class="fa fa-sliders"></i> Filter</button>

                    <?php echo form_close();



                    ?>



                    <!--                    this the end of the filters   -->
                </div>
            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_1">

                    <thead>
                    <tr>
                        <th width="2">#</th>
                        <th> Name </th>
                        <th> DOB </th>
                        <th> Country </th>
                        <th> City </th>

                        <th> Phone </th>
                        <th> Branch </th>
                        <th> Role </th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php



                    $no=1;
                    foreach($t as $user): ?>
                    <tr>
                        <td><?php echo $no; ?></td>
                        <td>
                            <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$user->id*date('Y'), ucwords($user->full_name)) ?>

                        </td>
                        <td> <?php echo date('d-m-Y',$user->dob); ?> </td>
                        <?php  $ct= $this->db->select('country')->from('country')->where('a2_iso',$user->country)->get()->row(); ?>
                        <td title="<?php echo $ct->country ?>"> <?php

                            echo character_limiter($ct->country,10) ?> </td>
                        <td> <?php
                            $st= $this->db->select('title')->from('state')->where('state',$user->city)->get()->row();
                            echo isset($st->title)?$st->title:'N/A' ?> </td>

                        <td> <?php echo $user->phone ?> </td>
                        <?php $br=$this->db->select('branch_name')->from('branch')->where('id',$user->branch_id)->get()->row(); ?>
                        <td> <?php echo isset($br->branch_name)?$br->branch_name:'N/A' ?> </td>
                        <td> <?php
                           $ut= $this->db->select('title')->from('user_type')->where('id',$user->user_type)->get()->row();
                            echo $ut->title ?> </td>

                        <td>
                            <?php


                            $status=$user->status;
                            $status_btn= '<div style="width:80px;" class="btn btn-sm btn-'.($status=='2'?'danger':'success').'">'.( $status=='2'?'Blocked':'Active').'</div>';


                            echo $status_btn;

                            ?>
                        </td>
                        <td><div class="btn-group">
                                <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'edit/'.$user->id*date('Y'),'  <i class="fa fa-pencil"></i> Edit') ?>
                                    </li>
                                    <?php if($this->session->userdata('user_type')!='2'){ ?>
                                    <li >

                                        <?php echo anchor($this->page_level.$this->page_level2.'delete/'.$user->id*date('Y'),'  <i class="fa fa-trash-o"></i> Delete','onclick="return confirm(\'Are you sure you want to delete ?\')"') ?>
                                    </li>
                                    <li>

                                        <?php echo $user->status==2? anchor($this->page_level.$this->page_level2.'unblock/'.$user->id*date('Y'),'  <i class="fa fa-check"></i> Unblock'): anchor($this->page_level.$this->page_level2.'ban/'.$user->id*date('Y'),'  <i class="fa fa-ban"></i> Ban' ,'onclick="return confirm(\'You are about to ban User from accessing the System \')"') ?>
                                    </li>
                                    <li class="divider">
                                    </li>
                                    <li>
                                        <?php echo anchor($this->page_level.$this->page_level2.'make_admin/'.$user->id*date('Y'),'  <i class="i"></i> Make admin') ?>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div></td>
                    </tr>
                    <?php $no++; endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

