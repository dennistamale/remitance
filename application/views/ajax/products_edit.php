<script>

    var EcommerceProductsEdit = function () {



    var handleReviews = function () {

    var grid = new Datatable();

    grid.init({
    src: $("#datatable_reviews"),
    onSuccess: function (grid) {
    // execute some code after table records loaded
    },
    onError: function (grid) {
    // execute some code on network or other general error
    },
    loadingMessage: 'Loading...',
    dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
    // So when dropdowns used the scrollable div should be removed.
    //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

    "lengthMenu": [
    [10, 20, 50, 100, 150, -1],
    [10, 20, 50, 100, 150, "All"] // change per page values here
    ],
    "pageLength": 10, // default record count per page
    "ajax": {
    "url": "<?php echo base_url() ?>demo/ecommerce_product_reviews.php", // ajax source
    },
    "columnDefs": [{ // define columns sorting options(by default all columns are sortable extept the first checkbox column)
    'orderable': true,
    'targets': [0]
    }],
    "order": [
    [0, "asc"]
    ] // set first column as a default sort by asc
    }
    });
    }

    var handleHistory = function () {

    var grid = new Datatable();

    grid.init({
    src: $("#datatable_history"),
    onSuccess: function (grid) {
    // execute some code after table records loaded
    },
    onError: function (grid) {
    // execute some code on network or other general error
    },
    loadingMessage: 'Loading...',
    dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options
    "lengthMenu": [
    [10, 20, 50, 100, 150, -1],
    [10, 20, 50, 100, 150, "All"] // change per page values here
    ],
    "pageLength": 10, // default record count per page
    "ajax": {
    "url": "<?php echo base_url() ?>demo/ecommerce_product_history.php", // ajax source
    },
    "columnDefs": [{ // define columns sorting options(by default all columns are sortable extept the first checkbox column)
    'orderable': true,
    'targets': [0]
    }],
    "order": [
    [0, "asc"]
    ] // set first column as a default sort by asc
    }
    });
    }



    return {

    //main function to initiate the module
    init: function () {
    handleReviews();
    handleHistory();
    }

    };

    }();

    jQuery(document).ready(function() {
    EcommerceProductsEdit.init();
    });

</script>