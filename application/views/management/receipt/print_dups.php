


<!-- BEGIN PAGE BASE CONTENT -->
<div class="invoice invoice-content-2 bordered custom_size">
    <div class="row invoice-logo" style="margin-bottom: 0px; border-bottom: solid thin grey;">
        <div class="col-xs-2 invoice-logo-space">
            <img src="<?php echo base_url('assets/favicon.png') ?>" style="width: 60px;" class="img-responsive" alt="" />
        </div>
        <div class="col-xs-7 invoice-payment" style="text-align: center;" >
            <h5 style="font-weight: bold"><?php echo strlen($cob->company)>0? ucwords($cob->company):'Wealth Money Transfer' ?></h5>
            <ul class="list-unstyled">
                <li>
                    <strong>Receipt No #:</strong> <?php echo str_pad($rc->id, 6, "0", STR_PAD_LEFT).'|'.date('dmY',$rc->created_on) ?> </li>
                <li hidden>
                    <strong>Account Name:</strong> Wealth Money Transfer </li>
                <li>
                    <strong>Branch:</strong> <?php echo strlen($cob->branch_name)>0?$cob->branch_name:'N/A' ; ?>  </li>
                <li>
                <li>
                    <strong>Street:</strong> <?php echo isset($cob->street)? ucwords($cob->street):'N/A' ?> </li>



            </ul>
        </div>
        <div style="font-size: smaller" class="col-xs-3">
            <ul class="list-unstyled" >

                <li style="margin-bottom: 5px;"> <?php echo '#'.str_pad($rc->id, 6, "0", STR_PAD_LEFT).' / '.date('d M, Y',$rc->created_on); ?>
                    <span class="muted"> </span>
                </li>
                <li style="margin-bottom: 5px;">Txn : <b><?php echo $rc->transaction_id ?> </b></li>

                <?php $document=$this->db->select('transaction_id,document_type,expiry_date,document_id')->from('recipient_details')->where('transaction_id',$id)->get()->row(); ?>

                <?php if( count($document)>0){ ?>
                    <li class="tootips" title="ID Type" style="margin-bottom: 5px;">

                        <?php echo strtoupper(humanize($document->document_type.' <b># '.$document->document_id.'</b>'));  ?>

                    </li>
                    <li style="margin-bottom: 5px;">

                        ID Expiry :    <?php echo date('d-m-Y', $document->expiry_date);  ?>

                    </li>
                    <li style="margin-bottom: 5px;">Status : <b style="font-size: larger"> <?php echo humanize($rc->status) ?> </b> </li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-6">
            <h6>Sender:</h6>
            <ul class="list-unstyled">
                <li> <?php $user=$this->db->select()->from('users')->where('id',$rc->sender_id)->get()->row();
                    echo  ucwords($user->full_name) ?> </li>
                <!--                <li> Mr Nilson Otto </li>-->
                <li>


                    <strong>Teller:</strong> <?php  $sender_teller=$this->db->select('full_name')->from('users')->where( array('id'=>$rc->created_by))->get()->row();
                    echo ucwords($sender_teller->full_name) ?> </li>

                <li>

                <li>
                    Branch :

                    <?php  echo isset($br->branch_name)? ucwords($br->branch_name):'N/A' ?>

                </li>
                <li>
                    Country :

                    <?php  echo ucwords($sender_country->country) ?>

                </li>

            </ul>
        </div>
        <div class="col-xs-6">
            <h6>Reciever: </h6>
            <ul class="list-unstyled">
                <li> <?php echo ucwords($rc->receiver_name); ?></li>
                <!--                <li> Mr Nilson Otto </li>-->
                <li>

                    <strong>Teller:</strong> <?php  $teller=$this->db->select('full_name')->from('users')->where(array('id'=>$rc->updated_by))->get()->row();
                    echo count($teller)>0?ucwords($teller->full_name):'N/A' ?>
                </li>



                <li>
                    Branch :

                    <?php  echo isset($receiver_branch->branch_name)? ucwords($receiver_branch->branch_name):'N/A' ?>

                    <?php if($subtitle=='sender'){ ?>
                        <span class="pull-right hidden"> Secret Code : <?php echo  $rc->secret_code ?> </span>
                    <?php } ?>
                </li>

                <li>
                    Country :
                    <?php echo isset($cty->country)? ucwords($cty->country):''; ?>


                </li>






            </ul>
        </div>


    </div>
    <div class="row" style="border-top: solid thin grey; padding-top: 10px;">
        <!--        this is the part for the computation-->
        <div class="col-xs-8">
            <table class="table table-condensed table-hover" style="margin-bottom: 0;">
                <thead>
                <tr>
                    <th width="2">#</th>
                    <th> Item </th>

                    <th> Currency </th>


                    <th> <span class="pull-right">Amount</span> </th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td style="">1.</td>
                    <td style=";"> <?php echo  $subtitle=='sender'?'Gross':'Net' ?> Amount </td>

                    <td  align="left" >  <?php echo  $rc->recipient_currency ?>  </td>

                    <td align="right">  <?php echo  number_format($rc->received_amount,2) ?> </td>
                </tr>

                <?php if($subtitle=='sender'){ ?>
                    <tr style="border-bottom: solid thin lightgrey;">
                        <td >2.</td>
                        <td> Commission </td>

                        <td  align="left" >  <?php echo  $rc->currency ?>  </td>

                        <td align="right" >  <?php echo  number_format($rc->commission,2) ?> </td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td> Other Charges </td>

                        <td  align="left"> <?php echo  $rc->currency ?>   </td>

                        <td align="right">  <?php echo  number_format($rc->other_charges,2) ?> </td>
                    </tr>
                <?php } ?>

                </tbody>

            </table>
            <div class="row">


                <div class="col-xs-8 invoice-block pull-right">
                    <ul class="list-unstyled amounts pull-right" style="font-weight: bold; font-size:larger;">
                        <?php if($subtitle=='sender'){ ?>
                            <li>
                                <strong>Forex rate: </strong> <?php echo  number_format($rc->forex_rate) ?></li>
                            <li>
                                <strong>Total amount: </strong>    <?php echo  $rc->currency.' '.number_format($rc->sent_amount,2) ?>
                            </li>
                        <?php }else{ ?>

                            <li>
                                <strong>Total amount: </strong>    <?php echo  $rc->recipient_currency.' '.number_format($rc->received_amount,2) ?>
                            </li>

                        <?php } ?>


                        <li hidden>
                            <strong>VAT:</strong> ----- </li>
                        <li hidden>
                            <strong>Grand Total:</strong> $12489 </li>
                    </ul>
                    <br/>



                </div>
            </div>
        </div>

        <!--        this is the part for the signature-->
        <div class="col-xs-4">



            <div class="row">
                <div class="col-md-12" style="margin-top: 5px;" >
                    <?php //echo  ucwords($teller->full_name) ?>Agent's Signature <br/>

                    <hr/>
                </div>
                <div class="col-md-12">
                    <?php echo $subtitle=='sender'?'Sender':'Receiver' ?>'s Signature <br/>
                    <hr/>
                </div>
                <div class="col-md-12">
                    <a href="javascript:void();" class="btn  blue hidden-print margin-bottom-" onclick="javascript: showElem(); window.print(); hideELem();"> Print
                        <i class="fa fa-print"></i>
                    </a>
                    <a class="btn <?php echo $rc->status=='canceled'?'red':'green'; ?>  hidden-print"><?php echo humanize($rc->status) ?>
                        <i class="fa fa-money"></i>
                    </a>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- END PAGE BASE CONTENT -->
<div style="border-bottom: dashed thin black; margin-top: 5px; margin-bottom: 10px;"></div>