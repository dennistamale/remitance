<!-- BEGIN PAGE CONTENT-->



<div class="row">
    <div class="col-md-12 col-sm-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-"></i>
                    <?php echo humanize($subtitle) ?>
                </div>
                <div class="actions">

                    <?php echo anchor($page_level.'billing_shipping/add_setup','<i class="fa fa-pencil"></i>Add Setup','class="btn btn-default btn-sm"'); ?>
                    <div class="btn-group">
                        <a class="btn btn-default btn-sm" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-cogs"></i> Tools <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-pencil"></i> Edit </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-trash-o"></i> Delete </a>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-ban"></i> Ban </a>
                            </li>
                            <li class="divider">
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="i"></i> Make admin </a>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="portlet-body">
                <table class="table table-striped table-bordered table-hover" id="sample_2">
                    <thead>
                    <tr>
                        <th class="table-checkbox">
                            <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes"/>
                        </th>
                        <th>
                            Username
                        </th>
                        <th>
                            Email
                        </th>
                        <th>
                            Status
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="odd gradeX">
                        <td>
                            <input type="checkbox" class="checkboxes" value="1"/>
                        </td>
                        <td>
                            shuxer
                        </td>
                        <td>
                            <a href="mailto:shuxer@gmail.com">
                                shuxer@gmail.com </a>
                        </td>
                        <td>
									<span class="label label-sm label-success">
									Approved </span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <input type="checkbox" class="checkboxes" value="1"/>
                        </td>
                        <td>
                            looper
                        </td>
                        <td>
                            <a href="mailto:looper90@gmail.com">
                                looper90@gmail.com </a>
                        </td>
                        <td>
									<span class="label label-sm label-warning">
									Suspended </span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <input type="checkbox" class="checkboxes" value="1"/>
                        </td>
                        <td>
                            userwow
                        </td>
                        <td>
                            <a href="mailto:userwow@yahoo.com">
                                userwow@yahoo.com </a>
                        </td>
                        <td>
									<span class="label label-sm label-success">
									Approved </span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <input type="checkbox" class="checkboxes" value="1"/>
                        </td>
                        <td>
                            user1wow
                        </td>
                        <td>
                            <a href="mailto:userwow@gmail.com">
                                userwow@gmail.com </a>
                        </td>
                        <td>
									<span class="label label-sm label-default">
									Blocked </span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <input type="checkbox" class="checkboxes" value="1"/>
                        </td>
                        <td>
                            restest
                        </td>
                        <td>
                            <a href="mailto:userwow@gmail.com">
                                test@gmail.com </a>
                        </td>
                        <td>
									<span class="label label-sm label-success">
									Approved </span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <input type="checkbox" class="checkboxes" value="1"/>
                        </td>
                        <td>
                            foopl
                        </td>
                        <td>
                            <a href="mailto:userwow@gmail.com">
                                good@gmail.com </a>
                        </td>
                        <td>
									<span class="label label-sm label-success">
									Approved </span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <input type="checkbox" class="checkboxes" value="1"/>
                        </td>
                        <td>
                            weep
                        </td>
                        <td>
                            <a href="mailto:userwow@gmail.com">
                                good@gmail.com </a>
                        </td>
                        <td>
									<span class="label label-sm label-success">
									Approved </span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <input type="checkbox" class="checkboxes" value="1"/>
                        </td>
                        <td>
                            coop
                        </td>
                        <td>
                            <a href="mailto:userwow@gmail.com">
                                good@gmail.com </a>
                        </td>
                        <td>
									<span class="label label-sm label-success">
									Approved </span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <input type="checkbox" class="checkboxes" value="1"/>
                        </td>
                        <td>
                            pppol
                        </td>
                        <td>
                            <a href="mailto:userwow@gmail.com">
                                good@gmail.com </a>
                        </td>
                        <td>
									<span class="label label-sm label-success">
									Approved </span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <input type="checkbox" class="checkboxes" value="1"/>
                        </td>
                        <td>
                            test
                        </td>
                        <td>
                            <a href="mailto:userwow@gmail.com">
                                good@gmail.com </a>
                        </td>
                        <td>
									<span class="label label-sm label-success">
									Approved </span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <input type="checkbox" class="checkboxes" value="1"/>
                        </td>
                        <td>
                            userwow
                        </td>
                        <td>
                            <a href="mailto:userwow@gmail.com">
                                userwow@gmail.com </a>
                        </td>
                        <td>
									<span class="label label-sm label-default">
									Blocked </span>
                        </td>
                    </tr>
                    <tr class="odd gradeX">
                        <td>
                            <input type="checkbox" class="checkboxes" value="1"/>
                        </td>
                        <td>
                            test
                        </td>
                        <td>
                            <a href="mailto:userwow@gmail.com">
                                test@gmail.com </a>
                        </td>
                        <td>
									<span class="label label-sm label-success">
									Approved </span>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>

</div>
<!-- END PAGE CONTENT-->