<div class="col-md-8">
    <!-- Begin: life time stats -->
    <div class="portlet light bordered">
        <div class="portlet-title hidden">
            <div class="caption">
                <i class="icon-share font-blue"></i>
                <span class="caption-subject font-blue bold uppercase">Transactions</span>
                <span class="caption-helper">Recent  report overview...</span>
            </div>

        </div>
        <div class="portlet-body" style="height: 462px;">
            <div class="tabbable-line">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#transactions" data-toggle="tab" class="font-green bold uppercase"> Recent Transactions </a>
                    </li>
                    <li>
                        <a href="#cancellation" data-toggle="tab"  class="font-green bold uppercase"> Cancellation </a>
                    </li>
                    <li>
                        <a href="#withdraws" data-toggle="tab"  class="font-green bold uppercase"> Withdraws </a>
                    </li>


                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="transactions">
                        <div class="table-responsive scroller" style="height:370px;">
                            <table class="table table-striped table-hover table-bordered">

                                <thead>
                                <tr>
                                    <th style="width: 140px;"> Txn ID </th>
                                    <th style="width: 140px;">Sender</th>
                                    <th style="white-space: nowrap;"> Net Amount </th>
                                    <th> Branch </th>
                                    <th style="width: 100px;">Status </th>
                                </tr>
                                </thead>

                                <tbody>
<?php foreach($this->db->select('a.id,a.sender_id,b.full_name,a.transaction_id,a.received_amount,a.secret_code,a.branch_id,a.status,a.recipient_currency,a.created_on')->from('transactions a')->join('users b','a.sender_id=b.id')->order_by('id','desc')->limit(20)->get()->result() as $tx) : ?>
                                <tr>
                                    <td>

                                <?php echo anchor($this->page_level.'cashout/process/'.date('Y')*$tx->id,(strlen($tx->transaction_id)>0?$tx->transaction_id:$tx->secret_code)) ?>

                                    </td>
                                    <td>

                                        <?php
                                        echo humanize($tx->full_name)
                                        ?>
                                    </td>
                                    <td align="right"> <?php echo$tx->recipient_currency.' '.number_format($tx->received_amount) ?> </td>

                                    <?php $bc=$this->db->select('branch_name')->from('branch')->where('id',$tx->branch_id)->get()->row(); ?>
                                    <td title="<?php echo $bc->branch_name ?>"> <?php

                                       echo word_limiter($bc->branch_name,2);

                                        ?> </td>
                                    <td>

                                        <?php
                                        $status=$tx->status;
                                        if($status=='cashed_out'){
                                        echo anchor($this->page_level.'cashout/process/'.date('Y')*$tx->id,'<i class="fa fa-search"></i> '.humanize($status),'class="btn btn-sm green-jungle" style="width:110px;"');
                                        }elseif($status=='pending'){
                                            echo anchor($this->page_level.'cashout/process/'.date('Y')*$tx->id,'<i class="fa fa-search"></i> '.humanize($status),'class="btn btn-sm btn-info" style="width:110px;"');
                                        }elseif($status=='hold'){
                                            echo anchor($this->page_level.'cashout/process/'.date('Y')*$tx->id,'<i class="fa fa-search"></i> '.humanize($status),'class="btn btn-sm dark" style="width:110px;"');
                                        }elseif($status=='not_approved'){
                                            echo anchor($this->page_level.'cashout/process/'.date('Y')*$tx->id,'<i class="fa fa-search"></i> '.humanize($status),'class="btn btn-sm btn-default" style="width:110px;"');
                                        }elseif($status=='canceled'){
                                            echo anchor($this->page_level.'cashout/process/'.date('Y')*$tx->id,'<i class="fa fa-search"></i> '.humanize($status),'class="btn btn-sm red" style="width:110px;"');
                                        }

                                        ?>
                                    </td>
                                </tr>
                                <?php endforeach ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="cancellation">
                        <div class="table-responsive scroller" style="height:370px;">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th> Txn ID </th>
                                    <th> Net Amount </th>
                                    <th> Branch </th>
                                    <th style="width: 80px;"> </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($this->db->select('id,transaction_id,received_amount,secret_code,branch_id,status,recipient_currency')->from('transactions')->where('status','canceled')->order_by('id','desc')->limit(20)->get()->result() as $tx) : ?>
                                    <tr>
                                        <td>

                                            <?php echo anchor($this->page_level.'cashout/process/'.date('Y')*$tx->id,(strlen($tx->transaction_id)>0?$tx->transaction_id:$tx->secret_code)) ?>

                                        </td>
                                        <td align="right"> <?php echo $tx->recipient_currency.' '.number_format($tx->received_amount) ?> </td>

                                        <?php $bc=$this->db->select('branch_name')->from('branch')->where('id',$tx->branch_id)->get()->row(); ?>
                                        <td title="<?php echo isset($bc->branch_name)?$bc->branch_name:'N/A' ?>"> <?php

                                            echo isset($bc->branch_name)?word_limiter($bc->branch_name,2):'N/A';

                                            ?> </td>
                                        <td>

                                            <?php
                                            $status=$tx->status;
                                            echo anchor($this->page_level.'cashout/process/'.date('Y')*$tx->id,'<i class="fa fa-search"></i> View','class="btn btn-sm green " style="width:110px;"');

                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="withdraws">
                        <div class="table-responsive scroller" style="height:370px;">
                            <table class="table table-striped table-hover table-bordered">
                                <thead>
                                <tr>
                                    <th> Txn ID </th>
                                    <th> Net Amount </th>
                                    <th> Branch </th>
                                    <th style="width: 80px;"> </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach($this->db->select('id,transaction_id,received_amount,secret_code,branch_id,status,recipient_currency')->from('transactions')->where('status','cashed_out')->order_by('id','desc')->limit(20)->get()->result() as $tx) : ?>
                                    <tr>
                                        <td>

                                            <?php echo anchor($this->page_level.'cashout/process/'.date('Y')*$tx->id,(strlen($tx->transaction_id)>0?$tx->transaction_id:$tx->secret_code)) ?>

                                        </td>
                                        <td align="right"> <?php echo $tx->recipient_currency.' '.number_format($tx->received_amount) ?> </td>

                                        <?php $bc=$this->db->select('branch_name')->from('branch')->where('id',$tx->branch_id)->get()->row(); ?>
                                        <td title="<?php echo isset($bc->branch_name)?$bc->branch_name:'N/A' ?>"> <?php

                                            echo isset($bc->branch_name)?word_limiter($bc->branch_name,2):'N/A';

                                            ?> </td>
                                        <td>

                                            <?php
                                            $status=$tx->status;
                                            echo anchor($this->page_level.'cashout/process/'.date('Y')*$tx->id,'<i class="fa fa-search"></i> View','class="btn btn-sm green" style="width:110px;"');

                                            ?>
                                        </td>
                                    </tr>
                                <?php endforeach ?>

                                </tbody>
                            </table>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <!-- End: life time stats -->
</div>