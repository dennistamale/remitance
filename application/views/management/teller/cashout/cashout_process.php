<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<!-- BEGIN PAGE LEVEL STYLES -->
<link href="<?php echo base_url() ?>assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css"/>


<!-- END PAGE LEVEL STYLES -->

<?php $trans=$this->db->select()->from('transactions')->where('id',$id)->get()->row() ?>



<div class="row">
    <div class="col-md-5">
        <!-- PORTLET MAIN -->
        <div class="portlet light bordered">
            <!-- STAT -->
<!--            id, sender_id, sent_amount, commission, received_amount, secret_code, sender_country, receiver_country, receiver_street_address, receiver_name, receiver_email, receiver_phone, receiver_gender, transfer_type, branch_id, created_on, created_by, updated_on, updated_by, status, id, id-->
            <div class="row list-separated profile-stat">

                <div class="col-md-6 col-sm-2 col-xs-6">
                    <div class="uppercase profile-stat-title" style="font-size: 20px;">  <?php echo  $trans->recipient_currency.' '.number_format($trans->received_amount,2) ?> </div>
                    <div class="uppercase profile-stat-text"> Net Amount </div>
                </div>
                <div class="col-md-6 col-sm-5 col-xs-6">
                    <div class="uppercase profile-stat-title" style="font-size: 20px;"> <?php echo ucwords($trans->secret_code); ?> </div>
                    <div class="uppercase profile-stat-text"> Secret Code </div>
                </div>

            </div>
            <!-- END STAT -->

            <div>
                <div class="row">
                    <?php if($trans->status=='pending'){ ?>
                    <div class="btn col-md-12 btn-info"> Transaction Status : <?php echo humanize($trans->status) ?></div>
                    <?php }elseif($trans->status=='cashed_out'){ ?>
                        <div class="btn col-md-12 green-jungle"> Transaction Status : <?php echo humanize($trans->status) ?></div>
                    <?php  }elseif($trans->status=='canceled'){ ?>
                        <div class="btn col-md-12 red"> Transaction Status : <?php echo humanize($trans->status) ?></div>
                    <?php } ?>
                </div>
                <div class="row">

                    <?php echo anchor($this->page_level.'receipt/sender/'.$id*date('Y'),'<i class="fa fa-print"></i> Sender Receipt',' class="btn btn-primary col-md-5 margin-top-10" style="color: white;"') ?>
                    <?php echo $trans->status=='cashed_out'?anchor($this->page_level.'receipt/cashout/'.$id*date('Y'),'<i class="fa fa-print"></i> Receiver Receipt',' class="btn btn-primary col-md-5 margin-top-10 pull-right" style="color: white;"'):''; ?>


                <?php if($trans->status=='not_approved'){ ?>

                    <div class="btn btn-danger  col-md-12 margin-top-10">Not approved yet</div>


                <?php }elseif($trans->status=='hold'){ ?>

                        <div class="btn dark  col-md-12 margin-top-10" style="color: red;" >On Hold (<?php echo $trans->hold_reason; ?>)</div>


                <?php  } ?>
                </div>
<hr/>
                <h4 class="profile-desc-title">
                    Txn ID : <b><?php  echo ($trans->transaction_id); ?> </b> </h4>

                <h4 class="profile-desc-title"> Sender :  <?php
                    $user=$this->db->select('full_name')->from('users')->where('id',$trans->sender_id)->get()->row();
                    echo isset($user->full_name)?anchor($this->page_level.'users/edit/'.$trans->sender_id*date('Y'), ucwords($user->full_name)):'N/A' ?> </h4>
                <h4 class="profile-desc-title"> Receiver :  <?php echo ucwords($trans->receiver_name); ?>
                     </h4>
                <span class="profile-desc-text"> Date Sent : <?php echo date('d-m-Y H:i',$trans->created_on) ?> </span>
                <hr/>

                <div class="margin-top-20 profile-desc-link">
                    <i class="fa fa-phone"></i>
                    Recipient Phone : <b><?php
                        echo $trans->receiver_phone ?> </b>
                </div>



                <div class="margin-top-20 profile-desc-link">
                    <i class="fa fa-inbox"></i>
                    Recipient Email : <b><?php
                        echo $trans->receiver_email ?> </b>
                </div>

                <div class="margin-top-20 profile-desc-link">
                    <i class="fa fa-globe"></i>Sent Country :<b> <?php
                        $c=$this->db->select('country')->from('country')->where('a2_iso',$trans->sender_country)->get()->row();
                        echo isset($c->country)?$c->country:'N/A'; ?></b>
                </div>
                <div class="margin-top-20 profile-desc-link">
                    <i class="fa fa-building"></i>
                    Branch Name : <b> <?php
                        $b=$this->db->select('branch_name')->from('branch')->where('id',$trans->branch_id)->get()->row();
                        echo isset($b->branch_name)?$b->branch_name:'N/A'; ?></b>

                </div>
                <div class="margin-top-20 profile-desc-link">
                    <i class="fa fa-user"></i>
                    Teller Name : <b><?php
                        $teller=$this->db->select('full_name')->from('users')->where('id',$trans->created_by)->get()->row();
                        echo anchor($this->page_level.'users/edit/'.$trans->created_by*date('Y'), ucwords($teller->full_name)) ?> </b>
                </div>


                <div class="margin-top-20 profile-desc-link">
                    <i class="fa fa-calendar"></i>
                    Expiry Date : <b><?php
                        echo date('d-m-Y',$trans->expiry_date) ?> </b>
                </div>

                <hr/>

                <div class="margin-top-20 profile-desc-link">
                    <i class="fa fa-exchange"></i>
                    Forex Rate : <b>
                        <?php echo $trans->forex_rate>1? number_format($trans->forex_rate,2): number_format($trans->forex_rate,6) ?> </b>
                </div>
                <div class="margin-top-20 profile-desc-link">
                    <i class="fa fa-money"></i>
                    Gross Amount : <b><?php
                        echo $trans->currency.' '.number_format($trans->sent_amount,2); ?> </b>
                </div>


                <div class="margin-top-20 profile-desc-link">
                    <i class="fa fa-money"></i>
                    commission: <b><?php
                        echo  $trans->currency.' '.number_format($trans->commission,3); ?> </b>
                </div>

                <div class="margin-top-20 profile-desc-link">
                    <i class="fa fa-money"></i>
                    Other Charges: <b><?php
                        echo  $trans->currency.' '.number_format($trans->other_charges,3); ?> </b>
                </div>


                <div class="margin-top-20 profile-desc-link">
                    <i class="fa fa-money"></i>
                    Net Amount: <b><?php
                        echo  $trans->recipient_currency.' '.number_format($trans->received_amount,2); ?> </b>
                </div>

            </div>
        </div>
        <!-- END PORTLET MAIN -->

    </div>




    <div class="col-md-7">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-directions font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> Recipient Details</span>
                </div>

                <div class="actions">
                    <?php if($trans->status=='cashed_out'){?>
                    <?php echo anchor($this->page_level.'receipt/cashout/'.$id*date('Y'),' <i class="icon-printer"></i> Receiver Receipt','class="btn btn-circle btn-primary btn-sm"'); ?>

                    <?php } ?>
                    <?php echo anchor($this->page_level.$this->page_level2,' <i class="icon-directions"></i> Cashout List','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">

                <?php echo form_open_multipart('',array('class'=>'form-horizontal')) ?>
                <!--                id, sender_id, sent_amount, commission, recieved_amount, secret_code, sender_country, reciever_country, reciever_street_address, reciever_name, reciever_email, reciever_phone, transfer_type, created_on, created_by, updated_on, updated_by, status, id, id-->
                <div class="form-body">
                    <div class="row">
<!--                    This is the Processing of the Uploading of the Documents-->
                    <div class="col-md-4">

<input type="hidden" name="txn_id" value="<?php echo $trans->transaction_id ?>">
                    <div class="form-group form-md-line-input">
                        <?php $doc=$this->db->select()->from('recipient_details')->where('transaction_id',$trans->id)->get()->row(); ?>
                        <?php if( isset($error)){?>
                            <span class="font-red-mint" >
                                        <?php echo  $error; ?>

                                    </span>
                        <?php } echo form_error('document','<label style="color:red;">','</label>') ?>

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 220px;">
                                <img src="<?php echo isset($doc->document)>0?base_url().$doc->document:base_url().'assets/user-id-icon.png' ?>" alt=""/>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 220px;">
                            </div>
                            <div>
																<span class="btn default btn-file">
																<span class="fileinput-new">
																Select image </span>
																<span class="fileinput-exists">
																Change </span>
																<input type="file" name="document">
																</span>
                                <a href="#" class="btn default fileinput-exists" data-dismiss="fileinput">
                                    Remove </a>
                            </div>
                        </div>

                    </div>
                    </div>
                        <div class="col-md-8">
                            <div class="form-group form-md-line-input">

                                <label class=" col-md-5 control-label">Document Type </label> <?php echo form_error('document_type','<label style="color:red;">','</label>') ?>


                                <div class="col-md-7">
                                    <select class="form-control" name="document_type" required>
                                        <option value="" <?php echo set_select('document_type', '', TRUE); ?> >Select Document Type</option>
                                        <option value="passport" <?php echo set_select('document_type', 'passport'); ?> >Passport</option>
                                        <option value="national_id" <?php echo set_select('document_type', 'national_id'); ?> >National ID</option>
                                        <option value="driving_permit" <?php echo set_select('document_type', 'driving_permit'); ?> >Driving
                                        <option value="refuge_id" <?php echo set_select('document_type', 'refuge_id'); ?> >Refuge ID</option>

                                    </select>
                                </div>

                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-5 control-label">Document ID </label> <?php echo form_error('document_id','<label style="color:red;">','</label>') ?>
                                <?php echo form_error('document_id','<label style="color:red;">','</label>') ?>
                                <div class="col-md-7">
                                    <input type="text" name="document_id"  placeholder="Insert the Document ID" required value="<?php echo set_value('document_id') ?>"  class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-5 control-label">Document Expiry Date </label> <?php echo form_error('document_expiry_date','<label style="color:red;">','</label>') ?>
                                <?php echo form_error('document_expiry_date','<label style="color:red;">','</label>') ?>
                                <div class="col-md-7">
                                        <input class="form-control form-control-inline input-medium date-picker" value="<?php echo set_value('document_expiry_date') ?>" required name="document_expiry_date"  type="text" size="4" width="10" />
                                        <span class="help-block"> Select the Expiry date for the Card </span>



                                </div>

                            </div>
                            <!--                    This is the end of the processing of the document-->

                        </div>

                    </div><div class="row">

                        <div class="col-md-12">




                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="form_control_1">Full Names</label>
                        <div class="col-md-9">
                            <input type="text"  required title="Enter the Full Names of the Receiver" value="<?php echo $trans->receiver_name ?>" name="receiver_name" class="form-control" readonly placeholder="Enter the Full Names of the Receiver" />
                            <div class="form-control-focus" style="color: red"><?php echo form_error('receiver_name') ?> </div>

                        </div>
                    </div>


                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="form_control_1">Country</label>
                        <div class="col-md-9">
                            <select class="form-control" name="country"  required >
                           
                                <?php foreach($this->db->select('country,dialing_code,a2_iso')->from('selected_countries')->get()->result() as $cat){ ?>
                                    <option value="<?php echo $cat->a2_iso  ?>"  <?php echo set_select('country', $cat->a2_iso, $cat->a2_iso==$trans->receiver_country?true:''); ?> ><?php echo $cat->country ?></option>
                                <?php } ?>

                            </select>
                            <label for="form_control_1"> <?php echo form_error('country','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Choose the Country</span>
                        </div>


                    </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">Branch</label>

                                <div class="col-md-9">
                                    <?php $br=$this->db->select('branch_name')->from('branch')->where('id',$trans->receiver_branch)->get()->row(); ?>
                                    <input type="text" readonly class="form-control" required name="street" value="<?php echo isset($br->branch_name)? $br->branch_name:'' ?>" id="form_control_1" placeholder="Street  address">
                                    <div class="form-control-focus" style="color: red"><?php echo form_error('street') ?> </div>
                                    <span class="help-block"></span>
                                </div>

                            </div>


                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="form_control_1">Receiver Email</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" name="receiver_email" value="<?php echo $trans->receiver_email ?>" id="form_control_1" placeholder="Receiver Email address">
                            <div class="form-control-focus" style="color: red"><?php echo form_error('receiver_email') ?> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                            <div class="form-group form-md-line-input">
                                <label class="col-md-3 control-label" for="form_control_1">2nd Phone Number</label>

                                <div class="col-md-9">
                                    <input type="text" disabled=""  class="form-control"  value="<?php echo $trans->receiver_phone ?>" id="form_control_1" >
                                    <div class="form-control-focus" style="color: red"> </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-3 control-label" for="form_control_1">2nd Phone Number</label>

                        <div class="col-md-9">
                            <input type="text"  class="form-control" name="phone2" value="<?php echo set_value('phone2') ?>" id="form_control_1" placeholder=" Phone Number(e.g 256xxxxxxxx)(Optional)">
                            <div class="form-control-focus" style="color: red"><?php echo form_error('phone') ?> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>



                        </div>

                    <div class="form-actions">

                        <div class="row">
                            <div class="col-md-offset-2 col-md-10">
                            <?php if($trans->status=='cashed_out'){?>
                                <div class="btn green-jungle   col-md-6">Transaction Processed Successfully</div>
                            <?php }elseif($trans->status=='not_approved'){ ?>
                                <div class="btn btn-danger col-md-offset-2  col-md-6">Not approved yet</div>
                            <?php }elseif($trans->status=='hold'){ ?>
                                <div class="btn dark col-md-offset-2  col-md-6">On Hold</div>

                           <?php  }else{
                                if($trans->created_by!=$this->session->userdata('id')){
                                ?>


                                <button type="reset" class="btn default"> <i class="fa fa-remove"></i> Reset</button>


                                <button type="<?php echo $trans->receiver_branch==$this->session->userdata('branch_id')?'submit':'button"';  ?>"
                                        data-original-title="Action not allowed"
                                        class="btn green-jungle <?php echo $trans->receiver_branch==$this->session->userdata('branch_id')?'':' tooltips disabled "';  ?>"  <?php  echo $trans->status=='canceled'?'disabled':'' ; ?>><i class="fa fa-money"></i> Process Payments</button>


                            <?php } } ?>


                                <?php




                                echo $trans->status=='cashed_out'?'':
                                    ($trans->status=='canceled'?'<div class="btn red" disabled><i class="fa fa-ban"></i> Canceled </div>'
                                        :
                                        ($trans->branch_id==$this->session->userdata('branch_id')?anchor($this->page_level.'transfer/cancel/'.$trans->id*date('Y'),'<i class="fa fa-ban"></i> Cancel Transaction',' class="btn red"'):'<div class="btn red disabled tooltips"  data-original-title="Action not allowed"><i class="fa fa-ban"></i>  Cancel Transaction</div>')) ?>
                            </div>
                        </div>
                    </div>
                        </div>

                    <?php echo form_close();   ?>
                </div>
            </div>
            <!-- END SAMPLE FORM PORTLET-->

        </div>


    </div>

    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->



    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>

<!--    --><?php // echo $trans->branch_id; ?>

