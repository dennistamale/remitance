<div class="row">
    <div class="col-md-12">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="fa fa-money font-green-haze"></i>
                    <span class="caption-subject bold uppercase">Add Commission</span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('') ?>
                <div class="form-body">

                    <?php
                    //'a.id,a.from_amount,a.to_amount,a.percentage,b.country'
                    $tax=$this->db->select('a.*,b.country as title')->from('taxes a')->join('selected_countries b','a.country=b.a2_iso')->where('id',$id)->get()->row(); ?>

                    <div class="row">




                        <div class="col-md-3">
                            <?php $co=$this->db->select()->from('selected_countries')->where('a2_iso != ',$tax->country)->get()->result() ?>
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="country" id="country" >
                                    <option value="<?php echo $tax->country ?>" <?php echo set_select('country', $tax->country, TRUE); ?> ><?php echo $tax->title ?></option>

                                    <?php foreach($co as $to){ ?>
                                        <option value="<?php echo $to->a2_iso  ?>"  <?php echo set_select('country', $to->a2_iso); ?> ><?php echo $to->country ?></option>
                                    <?php } ?>

                                </select>
                                <label for="form_control_1">Country </label><?php echo form_error('country','<row style=" color:red;">','</row>') ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group form-md-line-input has-success">
                                <input type="text"  class="form-control" name="name" value="<?php echo $tax->name ?>" placeholder="Tax Name">
                                <label for="form_control_1">Name  <?php echo form_error('name','<span style=" color:red;">','</span>') ?></label>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="type"  >
                                    <option value="" <?php echo set_select('type', '', TRUE); ?> >Select type</option>
                                    <option value="local" <?php echo set_select('type', 'local',$tax->type=='local'?true:''); ?> >Local</option>
                                    <option value="foreign" <?php echo set_select('type', 'foreign',$tax->type=='foreign'?true:''); ?> >Foreign</option>
                                </select>
                                <label for="form_control_1">Type </label><?php echo form_error('type','<row style=" color:red;">','</row>') ?>
                            </div>
                        </div>


                        <div class="col-md-2">
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="taxed"  >
                                    <option value="" <?php echo set_select('taxed', ''); ?> >Select what is taxed</option>
                                    <option value="gross" <?php echo set_select('taxed', 'gross' ,$tax->taxed=='gross'?true:''); ?> >Gross</option>
                                    <option value="commission" <?php echo set_select('taxed', 'commission'  ,$tax->taxed=='commission'?true:''); ?> >Commission</option>
                                </select>
                                <label for="form_control_1">Taxed </label><?php echo form_error('taxed','<row style=" color:red;">','</row>') ?>
                            </div>
                        </div>
                        <div class="col-md-2 hidden">
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="units"  >
                                    <option value="" <?php echo set_select('units', ''); ?> >Select Units</option>
                                    <option value="amount" <?php echo set_select('units', 'amount'); ?> >Amount</option>
                                    <option value="percent" <?php echo set_select('units', 'percent',true); ?> >Percentage</option>
                                </select>
                                <label for="form_control_1">Units </label><?php echo form_error('units','<row style=" color:red;">','</row>') ?>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group form-md-line-input has-success">
                                <input type="text" class="form-control" name="unit_value" value="<?php echo $tax->unit_value ?>" placeholder="Percentage Value">
                                <label for="form_control_1">Percentage Value  <?php echo form_error('unit_value','<span style=" color:red;">','</span>') ?></label>
                            </div>
                        </div>


                    </div>

                </div>
                <div class="form-actions">
                    <button type="submit" class="btn blue">Submit</button>
                    <button type="button" class="btn default">Cancel</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>

</div>
<?php $this->load->view($this->page_level.$this->page_level2.'taxes/taxes_js') ?>