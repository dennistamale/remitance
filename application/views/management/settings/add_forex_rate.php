<div class="row">
    <div class="col-md-12">

        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="fa fa-money font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> Forex Rates</span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">
                <?php echo form_open('') ?>
                <div class="form-body">
                    <?php echo form_error('to','<row style=" color:red;">','</row>') ?>
                    <div class="row">

                        <?php $co=$this->db->select('currency_alphabetic_code,currency_name,country')->from('country')->order_by('currency_alphabetic_code','asc')->get()->result() ?>

                        <div class="col-md-4">
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="from"  >
                                    <option value="" <?php echo set_select('from', '', TRUE); ?> >From</option>

                                    <?php foreach( $co as $from){ ?>
                                        <option value="<?php echo $from->currency_alphabetic_code  ?>"  <?php echo set_select('from', $from->currency_alphabetic_code); ?> ><?php echo $from->currency_name.' ('.$from->country.')'; ?></option>
                                    <?php } ?>

                                </select>
                                <label for="form_control_1"><b>1</b> <?php echo form_error('from','<span style=" color:red;">','</span>') ?></label>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="to"  >
                                    <option value="" <?php echo set_select('to', '', TRUE); ?> >To</option>

                                    <?php foreach($co as $to){ ?>
                                        <option value="<?php echo $to->currency_alphabetic_code  ?>"  <?php echo set_select('to', $to->currency_alphabetic_code); ?> ><?php echo $to->currency_name.' ('.$to->country.')' ?></option>
                                    <?php } ?>

                                </select>
                                <label for="form_control_1">To </label>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group form-md-line-input has-success">
                                <select class="form-control" name="country"  >
                                    <option value="" <?php echo set_select('from', '', TRUE); ?> >Country </option>

                                    <?php foreach( $this->db->select()->from('selected_countries')->get()->result() as $sc){ ?>
                                        <option value="<?php echo $sc->a2_iso  ?>"  <?php echo set_select('from', $sc->a2_iso); ?> ><?php echo $sc->country; ?></option>
                                    <?php } ?>

                                </select>
                                <label for="form_control_1" ><b>Country Forex Rate</b> <?php echo form_error('from','<span style=" color:red;">','</span>') ?></label>
                            </div>
                        </div>







                    </div>
                    <div class="row">

                        <div class="col-md-4">

                            <div class="form-group form-md-line-input has-success">
                                <input type="text" class="form-control" name="amount" value="<?php echo set_value('amount') ?>" placeholder="Amount">
                                <label for="form_control_1 " style="color: green; font-weight: bold;">Direct Amount  <?php echo form_error('amount','<span style=" color:red;">','</span>') ?></label>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="form-group form-md-line-input has-success">
                                <input type="text" class="form-control" name="reverse_amount" value="<?php echo set_value('reverse_amount') ?>" placeholder="Amount">
                                <label for="form_control_1 bold" style="color: orangered; font-weight: bold;">Reverse Amount  <?php echo form_error('reverse_amount','<span style=" color:red;">','</span>') ?></label>
                            </div>
                        </div>





                    </div>

                </div>
                <div class="form-actions">
                    <button type="submit" class="btn blue">Submit</button>
                    <button type="button" class="btn default">Cancel</button>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>

</div>

<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script>
    $('input[name="amount"]').keyup(function () {
        var direct_amount = $('input[name="amount"]').val();

        if(direct_amount!='') {
            var reverse_amount = 1 / direct_amount;
            $('input[name="reverse_amount"]').val(reverse_amount);
        }else{
            $('input[name="reverse_amount"]').val('');
        }
    });
</script>