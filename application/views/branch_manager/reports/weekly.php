<!-- BEGIN PAGE CONTENT-->
<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />

<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="row">
                    <div class="col-md-6">
                        <div class="btn-group">
                          <h4 class="bold">Weekly Report</h4>
                        </div>
                    </div>
                    <div class="col-md-6 hidden-print">
                        <div class="btn-group pull-right">
                            <button class="btn green  btn-outline " data-toggle="dropdown"  onclick="javascript:window.print();"><i class="fa fa-print"></i> Print
                            </button>

                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body" >


                <table class="table table-scrollable table-bordered table-hover" width="100%" border="0"  >

                    <tr style="font-weight: bold; background:#0C0;">
                        <td colspan="5" align="center">MONEY REMITANCE WEEKLY RETURN;</td>
                    </tr>
                    <tr style="font-weight: bold;">
                        <td style="font-weight: bold;">INSTITUTION NAME</td>
                        <td><?php echo $this->site_options->title('site_name') ?></td>
                        <td>&nbsp;</td>
                        <td style="font-weight: bold;">Instutution Code</td>
                        <td><?php echo $this->site_options->title('central_bank_code') ?></td>

                    </tr>
                    <tr>
                        <td style="font-weight: bold;">DAY(SATURDAY OF REPORTING WEEK)</td>
                        <td><?php echo date('d',strtotime("next Saturday")), "\n"; ?></td>
                        <td>&nbsp;</td>
                        <td style="font-weight: bold;">Financial Year</td>
                        <td><?php echo date('Y') ?></td>

                    </tr>
                    <tr>
                        <td style="font-weight: bold;">MONTH</td>
                        <td><?php echo date('F',strtotime("next Saturday")), "\n"; ?></td>
                        <td>&nbsp;</td>
                        <td>Start Date</td>
                        <td><?php echo date('d-m-Y',strtotime("last Monday")), "\n"; ?></td>

                    </tr>
                    <tr>
                        <td style="font-weight: bold;">YEAR</td>
                        <td><?php echo date('Y',strtotime("next Saturday")), "\n"; ?></td>
                        <td>&nbsp;</td>
                        <td>End Date</td>
                        <td><?php echo date('d-m-Y',strtotime("next Sunday")), "\n"; ?></td>

                    </tr>
                    <tr>
                        <td style="font-weight: bold;">DATE-SATURDAY OF REPORTING WEEK</td>
                        <td><?php echo date('d-m-Y',strtotime("next Saturday")), "\n"; ?></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>

                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Officer's Name</td>
                        <td> <?php echo $this->site_options->title('officer_name') ?></td>

                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Title</td>
                        <td> <?php echo $this->site_options->title('officer_title') ?></td>

                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Telephone Number</td>
                        <td> <?php echo $this->site_options->title('officer_phone') ?></td>

                    </tr>
                    <tr>
                    <td rowspan="2" style="font-weight: bold;">CURRENCY</td>
                        
                        <td colspan="2" style="font-weight: bold; background:#0099FF;">INFLOWS(RECIEVE)</td>
                        <td colspan="2"  style="font-weight: bold; background:#FF0000;">OUTFLOWS(SEND)</td>

                    </tr>
                    <tr>
                        <td>TOTAL</td>
                        <td>UGX EQUIVALENT</td>
                        <td>TOTAL</td>
                        <td>UGX EQUIVALENT</td>

                    </tr>
                   
                    <tr>

                        <td style="font-weight: bold;">USD</td>
                        <td>
<!--                            total usd-->
                            <?php
                            //these are global variables
                            $current_country=$this->session->userdata('country');
                            $cc=$this->db->select('currency_alphabetic_code')->from('country')->where('a2_iso',$current_country)->get()->row();
                            $current_currency=$cc->currency_alphabetic_code;

                            // this is the end of the global variables


                            $this->db->select_sum('received_amount')->from('transactions');
                            $this->db->where(array(
                                'recipient_currency'=>'USD',
                                'receiver_country'=>$current_country,
                                'created_on >=' => $first_day,
                                'created_on <=' => $last_day,
                                ) );
                            $tt_usd= $this->db->get()->row();
                           echo number_format($tt_usd->received_amount,2);
                            ?>
                        </td>
                        <td>
<!--                            this is the amount in the currrent country-->
                            <?php

                          $forex_usd=$this->db->select('to_amount')->from('forex_settings')->where(array('from_currency'=>'USD','to_currency'=>$current_currency))->get()->row();

                            echo number_format($tt_usd->received_amount*$forex_usd->to_amount,2);

                            ?></td>
                        <td><!--                            total usd-->
                            <?php

                            $this->db->select_sum('sent_amount')->from('transactions');
                            $this->db->where(array(
                                'currency'=>'USD',
                                'sender_country'=>$current_country,
                                'created_on >=' => $first_day,
                                'created_on <=' => $last_day,
                            ) );
                            $tt_usd= $this->db->get()->row();
                            echo number_format($tt_usd->sent_amount,2);
                            ?></td>
                        <td><?php    echo number_format($tt_usd->sent_amount*$forex_usd->to_amount,2); ?></td>

                    </tr>
                    <tr>

                        <td style="font-weight: bold;">GBP</td>
                        <td>
                            <!--                            total gbp-->
                            <?php

                            $this->db->select_sum('received_amount')->from('transactions');
                            $this->db->where(array(
                                'recipient_currency'=>'GBP',
                                'receiver_country'=>$current_country,
                                'created_on >=' => $first_day,
                                'created_on <=' => $last_day,
                            ) );
                            $tt_gbp= $this->db->get()->row();
                            echo number_format($tt_gbp->received_amount,2);
                            ?>
                        </td>
                        <td>
                            <!--                            this is the amount in the currrent country-->
                            <?php

                            $forex_gbp=$this->db->select('to_amount')->from('forex_settings')->where(array('from_currency'=>'GBP','to_currency'=>$current_currency))->get()->row();

                            $forex_gbp->to_amount;



                            echo number_format($tt_gbp->received_amount*$forex_gbp->to_amount,2);

                            ?></td>
                        <td><!--                            total usd-->
                            <?php

                            $this->db->select_sum('sent_amount')->from('transactions');
                            $this->db->where(array(
                                'currency'=>'GBP',
                                'sender_country'=>$current_country,
                                'created_on >=' => $first_day,
                                'created_on <=' => $last_day,
                            ) );
                            $tt_gbp= $this->db->get()->row();
                            echo number_format($tt_gbp->sent_amount,2);
                            ?></td>
                        <td><?php    echo number_format($tt_gbp->sent_amount*$forex_gbp->to_amount,2); ?></td>

                    </tr>
                    <tr>

                        <td style="font-weight: bold;">EUR</td>
                        <td>
                            <!--                            total usd-->
                            <?php


                            $this->db->select_sum('received_amount')->from('transactions');
                            $this->db->where(array(
                                'recipient_currency'=>'EUR',
                                'receiver_country'=>$current_country,
                                'created_on >=' => $first_day,
                                'created_on <=' => $last_day,
                            ) );
                            $tt_eur= $this->db->get()->row();
                            echo number_format($tt_eur->received_amount,2);
                            ?>
                        </td>
                        <td>
                            <!--                            this is the amount in the currrent country-->
                            <?php

                            $forex_eur=$this->db->select('to_amount')->from('forex_settings')->where(array('from_currency'=>'EUR','to_currency'=>$current_currency))->get()->row();

                            echo number_format($tt_eur->received_amount*$forex_eur->to_amount,2);

                            ?></td>
                        <td><!--                            total usd-->
                            <?php

                            $this->db->select_sum('sent_amount')->from('transactions');
                            $this->db->where(array(
                                'currency'=>'EUR',
                                'sender_country'=>$current_country,
                                'created_on >=' => $first_day,
                                'created_on <=' => $last_day,
                            ) );
                            $tt_eur= $this->db->get()->row();
                            echo number_format($tt_eur->sent_amount,2);
                            ?></td>
                        <td><?php    echo number_format($tt_eur->sent_amount*$forex_eur->to_amount,2); ?></td>

                    </tr>
                    <tr>

                        <td style="font-weight: bold;">OTHERS(USD EQUIV)</td>
                        <td>
                            <!--                            total usd-->
                            <?php

                            $tt_inflows=$this->db->query("SELECT SUM(`received_amount_usd`) AS received_amount_usd FROM (`transactions`) WHERE `recipient_currency` != 'GBP' AND `recipient_currency` != 'USD' AND `recipient_currency` != 'EUR' AND `recipient_currency` != '' AND `sender_country` = '$current_country' AND `created_on` >= $first_day AND `created_on` <= $last_day ;")->row();
                            echo number_format($tt_inflows->received_amount_usd,2);
                            ?>
                        </td>
                        <td>
                            <!--                            this is the amount in the currrent country-->
                            <?php

                            $forex_other=$this->db->select('to_amount')->from('forex_settings')->where(array('from_currency'=>'USD','to_currency'=>$current_currency))->get()->row();

                            echo number_format($tt_inflows->received_amount_usd*$forex_other->to_amount,2);

                            ?></td>
                        <td><!--                            total usd-->
                            <?php

                            $tt_outflows=$this->db->query("SELECT SUM(`sent_amount_usd`) AS sent_amount_usd FROM (`transactions`) WHERE `currency` != 'GBP' AND `currency` != 'USD' AND `currency` != 'EUR' AND `currency` != '' AND `sender_country` = '$current_country' AND `created_on` >= $first_day AND `created_on` <= $last_day ;")->row();
                            echo number_format($tt_outflows->sent_amount_usd,2);
                            ?></td>
                        <td><?php    echo number_format($tt_outflows->sent_amount_usd*$forex_other->to_amount,2); ?></td>

                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>

                    </tr>
                    <tr>
                        <td colspan="5">This Return should be submitted not later than the first working day after the end of the week to which it pertains</td>

                    </tr>
                    <tr>
                        <td colspan="5"><strong>Note: </strong>Other Currencies remitted should be converted to dollars and reported under the others but in the USD equivalent </td>

                    </tr>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->

    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->