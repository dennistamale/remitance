<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


<div class="row hidden-print">
    <div class="col-md-12">

<!--        --><?php //$branch=$this->custom_library->update_statement(1000,$this->uri->segment(3));
//
//        print_r($branch);
//        ?>
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-user font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> <?php echo $this->uri->segment(3); ?></span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <!--                    --><?php //echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">

                <?php echo form_open('',array('class'=>'form-horizontal')) ?>
                <!--                id, full_name, city, password, username, region, country, phone, email, gender, photo, user_type, sub_type, status, verified, created_on, created_by, updated_on, updated_by, id, id-->
                <div class="form-body">



                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Amount(USD)</label>
                        <div class="col-md-10">
                            <input name="amount" type="number" class="form-control" step="any" placeholder="Amount(USD)">

                            <label for="form_control_1"> <?php echo form_error('amount','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Choose the Country</span>
                        </div>

                    </div>


                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="reset" class="btn default"> <i class="fa fa-remove"></i> Cancel</button>
                            <button type="submit" class="btn green-jungle"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>


</div>

<!-- BEGIN CORE PLUGINS -->
<script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
