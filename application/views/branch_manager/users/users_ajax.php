<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">

        <!-- Begin: life time stats -->
        <div class="portlet light portlet-fit portlet-datatable bordered">

            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <!--this is the begining of the filters-->
                        <?php echo form_open($this->page_level.$this->page_level2.'filter','class="form-inline"') ?>
                        <?php echo anchor($this->page_level.$this->page_level2.'new',' <i class="fa fa-plus"></i> New','class="btn green-jungle"'); ?>

                        <div class="form-group">
                            <?php echo form_error('country','<label style="color:#ff0000;">','</label>'); ?>
                            <select class="form-control" name="country" style="width: 100px;">
                                <option value="" <?php echo set_select('country', '', TRUE); ?> >Country</option>
                                <?php foreach($this->db->select('a2_iso,country')->from('selected_countries')->get()->result() as $cty): ?>
                                    <option value="<?php echo $cty->a2_iso ?>" <?php echo set_select('country', $cty->a2_iso); ?> ><?php echo $cty->country ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>

                        <?php if($this->session->userdata('user_type')!='2'){ ?>

                            <div class="form-group">
                                <?php echo form_error('role','<label style="color:#ff0000;">','</label>'); ?>
                                <select class="form-control" name="role" style="width: 80px;" >
                                    <option value="" <?php echo set_select('role', '', TRUE); ?> >Role</option>
                                    <?php foreach($this->db->select('id,title')->from('user_type')->get()->result() as $role): ?>
                                        <option value="<?php echo $role->id  ?>" <?php echo set_select('role', $role->id); ?> ><?php echo humanize($role->title) ?></option>
                                    <?php endforeach; ?>



                                </select>
                            </div>

                        <?php } ?>

                        <button type="submit" class="btn btn-primary"><i class="fa fa-sliders"></i> Filter</button>

                        <div class="form-group hidden">
                            <input class="form-control" type="text" id="search" name="search" style="width: 100px;" placeholder="Search">
                        </div>

                        <?php echo form_close(); ?>




                        <!--                    this the end of the filters   -->

                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="users">
                        <thead>
                        <tr role="row" class="heading">
                            <th width="1%">
                                <input type="checkbox" class="group-checkable"> </th>
                            <th width="2%"># </th>
                            <th> Name </th>
                            <th> DOB </th>
                            <th width="10%"> Country </th>
                            <th> City </th>
                            <th> Phone </th>
                            <th> Branch </th>
                            <th> Role </th>
                            <th width="5%">Action</th>
                        </tr>

                        </thead>
                        <tbody> </tbody>

                        <tfoot>
                        <tr role="row" class="heading">
                            <th width="1%">
                                <input type="checkbox" class="group-checkable"> </th>
                            <th width="2%"># </th>
                            <th> Name </th>
                            <th> DOB </th>
                            <th width="10%"> Country </th>
                            <th> City </th>

                            <th> Phone </th>
                            <th> Branch </th>
                            <th> Role </th>
                            <th width="5%">Action</th>
                        </tr>

                        </tfoot>

                    </table>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<!-- BEGIN CORE PLUGINS -->

<?php $this->load->view('ajax/users') ?>