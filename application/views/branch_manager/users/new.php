<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN SAMPLE FORM PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-green-haze">
                    <i class="icon-user font-green-haze"></i>
                    <span class="caption-subject bold uppercase"> New User</span>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
                    <?php echo anchor($this->page_level.$this->page_level2,' <i class="fa fa-users"></i> Users','class="btn btn-circle btn-warning btn-sm"'); ?>
                </div>
            </div>
            <div class="portlet-body form">

                <?php echo form_open('',array('class'=>'form-horizontal')) ?>
<!--                id, full_name, city, password, username, region, country, phone, email, gender, photo, user_type, sub_type, status, verified, created_on, created_by, updated_on, updated_by, id, id-->
                <div class="form-body">
                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Full Name</label>
                        <div class="col-md-10">
                            <input type="text" required title="Enter Full Name" value="<?php echo set_value('full_name') ?>" name="full_name" class="form-control" placeholder="Enter Full Name" />
                            <div class="form-control-focus" style="color: red"><?php echo form_error('full_name') ?> </div>

                        </div>
                    </div>

                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Date of Birth</label>
                        <div class="col-md-8">
                            <div class="form-group form-md-line-input">
                                <div class="input-group  date date-picker" data-date-format="yyyy-mm-dd">
                                    <input type="text" class="form-control" name="dob" value="<?php echo set_value('dob') ?>">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                    <label for="form_control_1"> <?php echo form_error('dob','<span style=" color:red;">','</span>') ?></label>

                                </div>

                            </div>
                        </div>

                    </div>

                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Email</label>
                        <div class="col-md-10">
                            <input type="email" class="form-control" required name="email" value="<?php echo set_value('email') ?>" id="form_control_1" placeholder="User Email address">
                            <div class="form-control-focus" style="color: red"><?php echo form_error('email') ?> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Zip Code / Phone Number</label>
                        <div class="col-md-2">
                            <select class="form-control" name="zip_code"  >
                                <option value="" <?php echo set_select('zip_code', '', TRUE); ?> >Select Zip Code</option>

                                <?php foreach($this->db->select('dialing_code,a2_iso')->from('country')->order_by('a2_iso','asc')->get()->result() as $cat){ ?>
                                    <option value="<?php echo $cat->dialing_code  ?>"  <?php echo set_select('zip_code', $cat->dialing_code); ?> ><?php echo $cat->a2_iso.'('.$cat->dialing_code.')' ?></option>
                                <?php } ?>

                            </select>
                            <label for="form_control_1"> <?php echo form_error('zip_code','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Choose the Zip code for your country</span>
                        </div>
                        <div class="col-md-8">
                            <input maxlength="10" type="text" class="form-control" required name="phone" value="<?php echo set_value('phone') ?>" id="form_control_1" placeholder="User Phone Number(e.g 07xxxxxx)">
                            <div class="form-control-focus" style="color: red"><?php echo form_error('phone') ?> </div>
                            <span class="help-block"></span>
                        </div>
                    </div>

                    <div class="form-group  form-md-radios">
                        <label class="col-md-2 control-label">Gender</label>
                        <div class="md-radio-inline  col-md-4">
                            <div class="md-radio">
                                <input type="radio" id="radio6" name="gender" value="M" <?php echo set_radio('gender','M'); ?> class="md-radiobtn">
                                <label for="radio6">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Male </label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="radio7" name="gender" value="F" <?php echo set_radio('gender','F'); ?> class="md-radiobtn">
                                <label for="radio7">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Female </label>
                            </div>

                        </div>
                        </div>



                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Branch</label>
                        <div class="col-md-10">
                            <?php $bran=$this->db->select('a.id,branch_name,a.country,a.state')->from('branch a')->get()->result(); ?>
                            <select class="form-control" name="branch"  >
                                <option value="" <?php echo set_select('branch', '', TRUE); ?> >Select Branch</option>

                                <?php foreach($bran as $cat){ ?>
                                    <option value="<?php echo $cat->id  ?>"  <?php echo set_select('branch', $cat->id); ?> >
                                        <?php $ctry=$this->db->select('country')->from('selected_countries')->where('a2_iso',$cat->country)->get()->row(); ?>
                                        <?php echo $cat->branch_name.'-'.$ctry->country ?>
                                    </option>
                                <?php } ?>

                            </select>
                            <label for="form_control_1"> <?php echo form_error('branch','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Choose the Branch</span>
                        </div>
                    </div>


                    <?php if($this->session->userdata('user_type')!='2'){ ?>

                    <div class="form-group form-md-line-input">
                        <label class="col-md-2 control-label" for="form_control_1">Role</label>
                        <div class="col-md-4">
                            <select class="form-control" name="role">

                                    <option value="" <?php echo set_select('role', '', TRUE); ?> >Select User Role</option>
                                <?php
                                $ut=$this->session->userdata('user_type');

                                $this->db->select('id,title')->from('user_type');
                                $ut==1||$ut==3?'':$this->db->where('id',2);
                                $role=$this->db->order_by('id','asc')->get()->result();
                                foreach($role as $role): ?>
                                    <option value="<?php echo $role->id ?>" <?php echo set_select('role', $role->id); ?> ><?php echo $role->title ?></option>
                                <?php  endforeach; ?>



                            </select>
                            <label for="form_control_1"> <?php echo form_error('country','<span style=" color:red;">','</span>') ?></label>
                            <span class="help-block">Choose the Country</span>
                        </div>

                        <label class="col-md-2 control-label">Access Control</label>
                        <div class="md-radio-inline  col-md-4">
                            <div class="md-radio">
                                <input type="radio" id="c" name="access" value="c" <?php echo set_radio('access','c'); ?> class="md-radiobtn">
                                <label for="c">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Country </label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="b" name="access" value="b" <?php echo set_radio('access','b'); ?> class="md-radiobtn">
                                <label for="b">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Branch </label>
                            </div>

                        </div>
                    </div>
                    <?php } ?>

                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-10">
                            <button type="reset" class="btn default"> <i class="fa fa-remove"></i> Cancel</button>
                            <button type="submit" class="btn green-jungle"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->

    </div>


</div>

    <!-- BEGIN CORE PLUGINS -->
    <script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?php echo base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="<?php echo base_url() ?>assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
