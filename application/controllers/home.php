<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller
{

    public $username = "";
    public $password = "";
    public $page_level = "";

    public $notification = array();

    public $view_data = array();

    function __construct()
    {
        parent::__construct();


        $this->page_level = strlen($this->uri->slash_segment(1)) > 1 ? $this->uri->slash_segment(1) : 'home/';
        $cur_page = current_url();
        $overtime = $this->overtime();
//        if(!strstr($cur_page,'admin')){
//            if($overtime){
//                if(!strstr($cur_page,'lock_screen')){
//                    $this->true_time();
//                }
//            }
//        }


    }

    public function time()
    {
        echo date('D M d Y H:i:s');
    }

    public function index($page = null)
    {
        $data['view'] = 'home';
        $data['sublink'] = '';
        $data['page'] = $page;

        $this->login();


    }

    function overtime()
    {

        return date('H', time()) >= 18 || date('H', time()) < 7 ? true : false;


    }

    function true_time()
    {
        redirect('home/lock_screen', 'refresh');
    }

    function lock_screen()
    {
        if ($this->overtime()) {
            $this->load->view('home/login/overtime');
        } else {
            $this->login();
        }

    }

    //this is the landing page
    function landing($type = null)
    {
        $page_level = $this->page_level;
        $data = array(
            'title' => 'welcome',
            'subtitle' => $type,
            'link_details' => 'Account overview',
            'page_level' => $page_level

        );
        $this->load->view($page_level . 'landing/header', $data);

        $this->load->view($page_level . 'landing/index');

        $this->load->view($page_level . 'landing/footer');


    }


    function delete()
    {
        $this->db->where('id', 1);
        $this->db->delete('users');
    }

    public function Ticket($length)
    {
        $token = $this->GetSMSCode($length);
        while (1) {
            if ($this->db->select('id')->from('transactions')->where(array('ticket_number' => $token))->get()->num_rows() == 0) {
                break;
            } else {
                $token = $this->GetSMSCode($length);
            }

        }

        return $token;
    }


    public function gettoken()
    {
        $token = $this->GetSMSCode(6);
        while (1) {
            if ($this->db->select('id')->from('transactions')->where(array('token' => $token))->get()->num_rows() == 0) {
                break;
            } else {
                $token = $this->GetSMSCode(6);
            }

        }

        return 'RYLCT' . $token;
    }


    public function verify_sms_code()
    {
        if (strlen($this->session->userdata('id')) > 0) {
            $phno = $this->session->userdata('mobile');
            $_REQUEST['mobile'] = strlen($phno) == 10 ? '256' . substr($phno, 1) : $phno;
            $_REQUEST['code'] = $_REQUEST['verification_code'];

            if ($this->db->select('id')->from('verification_codes')->where(array('mobile' => $_REQUEST['mobile'], 'status' => 0, 'code' => $_REQUEST['code']))) {
                //$this->db->where('mobile = "'.$_REQUEST['mobile'].'" and code = "'.$_REQUEST['code'].'"');
                $this->db->update('verification_codes', array('status' => 1), array('mobile' => $_REQUEST['mobile'], 'code' => $_REQUEST['code']));
                $this->db->update('users', array('username' => $_REQUEST['mobile'], 'mobile_verified' => 1, 'status' => 1), array('mobile' => $_REQUEST['mobile']));

                $data['mobile_verified'] = 1;

                $data['SESSION_ID'] = sha1(date('Y-m-d H:i:s') . $_REQUEST['mobile']);
                $this->session->set_userdata($data);

                $this->db->insert('sessions',
                    array('session_id' => $data['SESSION_ID'],
                        'created_at' => date('Y-m-d H:i:s'),
                        'mobile' => $_REQUEST['mobile']));


                $data['status'] = 8;
                $data['message'] = 'Your account has been verified successfully';
            } else {


                $data['status'] = 9;
                $data['message'] = 'You have used an unknown code to activate';
            }


            redirect('/home/myaccount', 'refresh');

        }

    }


    public function search($date = null, $page = null)
    {

        $data['view'] = 'home';
        $data['sublink'] = 'search';
        $data['message'] = '';
        $this->form_validation->set_rules('search', 'Search', 'xss_clean|trim');
        if ($this->form_validation->run() == true) {
            $data['src'] = $this->input->post('search');
        }
        strlen($date) > 0 ? $data['src'] = $date : '';
        $data['page'] = $page;
        include('cal_pref.php');
        $this->load->library('calendar', $prefs);
        $this->load_home($data);

        //$data['page'] = $page;

    }


    public function request_code()
    {

        if (strlen($this->session->userdata('id')) > 0) {

            $verification_code = $this->GetSMSCode(5);


            $this->db->where('mobile = "' . $this->session->userdata('mobile') . '" and status = 0');
            $this->db->update('verification_codes', array('status' => 2));
            $this->db->insert('verification_codes',
                array('mobile' => $this->session->userdata('mobile'),
                    'code' => $verification_code,
                    'created_at' => date('Y-m-d H:i:s')));

            $message = 'ePay Verification Code : ' . $verification_code;


            $values = array(
                'created' => date('Y-m-d H:i:s'),
                'sender' => 'ePay',
                'receiver' => $this->session->userdata('mobile'),
                'message' => $message,
                'type' => 'VERIFICATION_CODE',
                'mobile' => $this->session->userdata('mobile'),
                'transactionid' => 0);

            $this->db->insert('outbox_messages', $values);


            $this->compticketmodel->SendSMS('ePay', $message, $this->session->userdata('mobile'));

            redirect('home', 'refresh');

        }
    }


    public function isloggedin()
    {
        if (strlen($this->session->userdata('id')) > 0) {
            return true;

        } else {
            return false;

        }

    }


    //checking whether the account exists
    public function account_exists($username, $password)
    {
        $status = 0;
        return $results = $this->db->select()->from('users')->where(array('username' => $username, 'password' => $this->hashValue($password)))->get()->row();

    }

    public function login()
    {


        $data['title'] = 'login';
        $data['subtitle'] = 'login';
        $uri = uri_string();

        //$this->load->view('admin/login');
        if (strlen($this->session->userdata('username')) > 0) {
            $user_type = $this->session->userdata('user_type');

            if ($user_type == 1) {
                redirect('admin', 'refresh');

            } elseif ($user_type == 2) {
                redirect('teller', 'refresh');
            } elseif ($user_type == 3) {
                redirect('management', 'refresh');
            } elseif ($user_type == 4) {
                redirect('accountant', 'refresh');
            }  elseif ($user_type == 6) {
            redirect('branch_manager', 'refresh');
        }
            else {
                $this->logout();
            }

        } else {

            if (isset($_REQUEST['username'])) {

                $this->form_validation->set_rules('username', 'Email', 'required|xss_clean|trim');
                $this->form_validation->set_rules('password', 'Password', 'required|xss_clean|trim');


                if ($this->form_validation->run() == true) {
                    $this->username = $this->input->post('username', true);
                    $this->password = $this->input->post('password', true);


                    $results = $this->account_exists($this->username, $this->password);


                    if (isset($results->username)) {

                        //0=blocked at country Level
                        //1=active at all levels
                        //2=blocked at a branch Level
                        $branch_status = $this->branch_status($results->branch_id);

                        if ($branch_status == 1 || $results->user_type == 1) {
                            //this check wetha the accounnt is not disabled
                            if ($results->status == 2) {
                                $data = array(
                                    'fullname' => $results->full_name,
                                    'username' => $results->username,
                                    'photo' => $results->photo
                                );
                                //echo 'account disabled';
                                $data['message'] = '<br/>Your Account is Currently Suspended <br/> <strong>Contact the Administrator</strong>';
                                $this->load->view($this->page_level . 'login/locked', $data);
                            } else {
                                //$amount=$this->db->select('current_amount')->from('wallet')->where('user',$results->id)->get()->row();
                                $branch_name = $this->db->select('branch_name')->from('branch')->where('id', $results->branch_id)->get()->row();
                                $session_data = array(
                                    'id' => $results->id,
                                    'username' => $results->username,
                                    'fullname' => $results->full_name,
                                    'photo' => $results->photo,
                                    'email' => $results->email,
                                    'user_type' => $results->user_type,
                                    'sub_type' => $results->sub_type,
                                    'country' => $results->country,
                                    'phone' => $results->phone,
                                    'branch_id' => $results->branch_id,
                                    'city' => $results->city,
                                    'access' => $results->access,
                                    'branch_name'=>$branch_name->branch_name,
                                    //'balance'=>$results->balance
                                   // 'verified'=>$results->verified

                                );


                                $this->session->set_userdata($session_data);
                                $this->db->where('id', $this->session->userdata('id'))->update('users', array('status' => '1'));
                                //this code redirects the users of the website
                                if ($results->user_type == 1) {
                                    redirect('admin', 'refresh');

                                } elseif ($results->user_type == 2) {
                                    redirect('teller', 'refresh');
                                } elseif ($results->user_type == 3) {
                                    redirect('management', 'refresh');
                                } elseif ($results->user_type == 4) {
                                    redirect('accountant', 'refresh');
                                }elseif ($results->user_type == 6) {
                                    redirect('branch_manager', 'refresh');
                                }

                                else {
                                    $this->logout();
                                }
                            }
                            //end of the account status check
                        } else {
                            if ($branch_status == 0) {
                                $data['message'] = '<b>Sorry!</b> Your Country has been blocked from Using the System.<br/> <b> Contact the Country Administrator</b>';
                                $this->load->view($this->page_level . 'login/login', $data);
                            } elseif ($branch_status == 2) {
                                $data['message'] = '<b>Sorry!</b> This branch has been blocked from Using the System.<br/> <b>Contact the Branch Manager</b>';
                                $this->load->view($this->page_level . 'login/login', $data);
                            } elseif ($branch_status == 3) {
                                $data['message'] = '<b>Sorry!</b> You have not been assigned to any branch .<br/> <b>Contact the Branch Manager</b>';
                                $this->load->view($this->page_level . 'login/login', $data);
                            }
                        }


                    } else {

                        //$message = 'Testing';

                        $data['message'] = 'Invalid Username or Password';
                        $this->load->view($this->page_level . 'login/login', $data);

                    }


                } else {

                    //$this->logout();
                    $data['message'] = 'All fields are Required';
                    $this->load->view($this->page_level . 'login/login', $data);

                }


            } else {

                //$this->logout();

                $this->load->view($this->page_level . 'login/login', $data);
            }

        }


        //$this->load->view('login');

    }

    //// this is the function which checks the status of the branch//////////////
    function branch_status($branch_id)
    {

        $bra = $this->db->select('id,status,country_status')->from('branch')->where('id', $branch_id)->get()->row();

        if (isset($bra->country_status)) {
            //0=blocked at country Level
            //1=active at all levels
            //2=blocked at a branch Level


            if ($bra->country_status == 1) {
                if ($bra->status == 'active') {
                    return 1;
                } else {
                    return 2;
                }
            } else {
                return 0;
            }

        } else {
            return 3;
        }
    }

    ///// This is the function which checks the status of the country


    function SendSMS($sender, $destination, $message, $type = '')
    {
        $email = 'denis@timesolut.com';
        $password = 'shawnluvu';
        $url = 'http://caltonmobile.com/calton/api.php?';
        $parameters = 'username=[EMAIL]&password=[PASSWORD]&contacts=[DESTINATION]&message=[MESSAGE]&sender=[SENDERID]';
        $parameters = str_replace('[EMAIL]', $email, $parameters);
        $parameters = str_replace('[PASSWORD]', urlencode($password), $parameters);
        $parameters = str_replace('[DESTINATION]', $destination, $parameters);
        $parameters = str_replace('[MESSAGE]', urlencode($message), $parameters);
        $parameters = str_replace('[SENDERID]', urlencode($sender), $parameters);
        $post_url = $url . $parameters;
        $response = file($post_url);
        $this->db->insert('outbox_messages', array(
            'created' => time(),
            'sender' => 'Virtual Wallet',
            'receiver' => $destination,
            'message' => $message,
            'type' => $type,
            'mobile' => $sender
        ));
        return $response[0];
    }
    //function sending an email

    //this of the password change
    function password_change($id, $phone)
    {


        $user = $data['user'] = $this->db->select()->from('users')->where(array('id' => $id / date('Y'), 'mobile' => $this->phone($phone)))->get()->row();
        if (isset($user->id)) {
            $data = array('view' => 'password_change', 'sublink' => 'password_change');
            $this->form_validation->set_rules('password', 'Password', 'trim|matches[passconf]|required|xss_clean');
            $this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|xss_clean');
            if ($this->form_validation->run() == false) {

            } else {
                $this->db->where(array('mobile' => $phone, 'id' => $id / date('Y')))->update('users', array('password' => $this->hashValue($this->input->post('password'))));

                $data['message'] = 'Your Password Has Been Updated Successfully. ' . anchor('home/login', 'Click Here to Login');
                $data['alert'] = 'success';
                $msg = 'Your account  Password Has been Changed ( ' . base_url() . ' )';
                //$this->SendSMS('Virtual Wallet',$phone,$msg);
                $this->sendemail($user->email, 'Password Reset', $msg);

            }
        } else {
            $data = array('view' => 'reset_password', 'sublink' => 'reset_password');
            $data['message'] = 'An error has Occurred Please Try again';
            $data['alert'] = 'warning';
        }

        $this->load_home($data);

    }

    // this is the function for the reseting the password
    function reset_password()
    {
        $data['view'] = 'reset_password';
        $data['sublink'] = 'reset_password';
        $this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|valid_email|callback_email_exist');
        if ($this->form_validation->run() == false) {
            //$this->load->view('account_reset');
        } else {
            $this->load->helper('text');
            $user = $this->db->select()->from('users')->where('email', $this->input->post('email'))->get()->row();
            $data['message'] = 'Check Your email to complete account Reset ';
            $data['alert'] = 'success';
            $email_msg = "Dear " . $user->fname . " " . $user->lname . " \n\r\n We Thank you for using Our Services Please follow the link below to reset the account \n\r\n" . site_url('/home/password_change/' . $user->id * date('Y') . '/' . $user->mobile) . "'>Follow This Link'";
            //$this->load->view('account_reset',$data);
            $this->sendemail($user->email, 'Virtual Wallet Account Reset', $email_msg);
        }
        $this->load_home($data);

    }

    //this is the function
    function email_exist($str)
    {
        $user = $this->db->select('email')->from('users')->where('email', $str)->get()->row();
        if (isset($user->email)) {
            return true;
        } else {
            $this->form_validation->set_message('email_exist', 'The %s you entered is wrong Please Try Again');
            return false;
        }
    }

    //this is the function for email verification
    function email_verification()
    {
        if (strlen($this->session->userdata('username')) > 0) {
            $data['title'] = 'Email Verification';
            $data['group'] = 'profile';
            $data['desc'] = 'Send an Admin a Message for any inquiry'; // the is the page description
            $this->load->view('header', $data);
            $this->load->helper('text');
            $user = $this->Octopusmodel->select_user($this->input->post('mobile'));
            $data['msg'] = 'Please Check Your email to complete account Verificaton ' . anchor('octopus', 'Go Back') . ' ' . $this->session->userdata('email');
            $email_msg = "Dear " . $this->session->userdata('fname') . " " . $this->session->userdata('lname') . " \n\r\n We Thank you for using Our Services Please follow the link below to Verify your email account \n\r\n" . site_url('/octopus/reg_complete/' . $this->session->userdata('id'));
            $this->sendemail($this->session->userdata('email'), 'Octopus Account Verification', $email_msg);
            //this shows the message alert
            $data['alert_type'] = 'info';
            $this->load->view('admin/alert', $data);
            $this->load->view('footer', $data);
        } else {
            $this->login();
        }
    }

    //this is the function for the has value
    public function hashValue($v)
    {
        return sha1(md5($v));
    }
    //this checks for the email weather it exist when signing up an account
    //checking for the valid number
    //checking for the username weather it exists in the database
    public function username_check($str)
    {
        $results = $this->db->select('username')->from('users')->where('email', $str)->get()->row();

        if (isset($results->username)) {
            $this->form_validation->set_message('username_check', 'The %s already Exists');
            return false;
        } else {
            return true;
        }
    }

    // this is the email check for the user
    public function email_check($str)
    {
        $results = $this->db->select('email')->from('users')->where('email', $str)->get()->row();

        if (isset($results->email)) {
            $this->form_validation->set_message('email_check', 'The %s already Exists');
            return false;
        } else {
            return true;
        }
    }

    function phone($phone)
    {
        if (strstr($phone, '+')) {
            $phone = substr($phone, 1);
            $mobile = strlen($phone) == 10 ? '256' . substr($phone, 1) : $phone;
            return $mobile;
        } else {
            $mobile = strlen($phone) == 10 ? '256' . substr($phone, 1) : $phone;
            return $mobile;
        }
    }

    function sendemail($reciever, $subject, $message)
    {

        $this->load->library('email');

        $config['protocol'] = 'smtp';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['useragent'] = 'Virtual Wallet';
        $config['smtp_host'] = 'www.timesolut.com';
        $config['smtp_user'] = 'no-reply@timesolut.com';
        $config['smtp_pass'] = 'b],PLIa~1y7h';
        $this->email->initialize($config);
        $this->email->from('no-reply@virtualwallet.com', 'Virtual Wallet');
        $this->email->to($reciever);
        $this->email->subject('Virtual Wallet | ' . $subject . ' Time >> ' . strftime("%T", time()));
        $this->email->message($message);
        if ($this->email->send()) {
            return true;
        }


    }

    // this is where sign up is suppossed to take place
    function signup()
    {
        $data['title'] = 'register';
        $data['subtitle'] = 'signup';

        //$data['code']=$this->Octopusmodel->country_code();
        $this->form_validation->set_rules('fullname', 'Full Name', 'trim|required|min_length[3]xss_clean');
        //$this->form_validation->set_rules('lastname', 'Last Name','trim|required|min_length[3]|max_length[20]|xss_clean');
        $this->form_validation->set_rules('email', 'Email Address', 'xss_clean|callback_email_check|trim|valid_email|required');
        $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required|min_length[10]|max_length[13]|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]');
        $this->form_validation->set_rules('username', 'Username', 'xss_clean|trim|is_unique[users.username]');
        $this->form_validation->set_rules('gender', 'Gender', 'xss_clean|trim|max_length[1]|required');
        $this->form_validation->set_rules('city', 'City', 'xss_clean|trim');
        //$this->form_validation->set_rules('dob','Date of Birth','xss_clean|trim|required');
        $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|trim|required');

        if ($this->form_validation->run() == false) {
            //$this->load->view('register',$data);

        } else {
            $data['msg'] = 'The Registration Process is completed Successfully <br/> <p> An Email Has been Sent For Verification to ' . $this->input->post('email') . ' </p>';
            //$this->load->view('loginmsg', $data);
            $v['full_name'] = $this->input->post('fullname');
            $username = $v['username'] = $this->input->post('username');
            $email = $v['email'] = strtolower($this->input->post('email'));
            $v['city'] = $this->input->post('city');
            $v['gender'] = $this->input->post('gender');
            $v['country'] = $this->input->post('country');
            $v['password'] = $this->hashValue($password = $this->input->post('password'));
            $v['phone'] = $this->phone($this->input->post('phone'));
            //$v['date_of_birth'] = $this->input->post('dob');
            $v['created_on'] = time();

            $this->db->insert('users', $v);


            $msg = $this->input->post('fullname') . ' Your account ' . $username . ' Has been created with us, Thank You for Registering ' . site_url();
            //$this->notification($username,$msg,$this->input->post('fname').' '.$this->input->post('lname'),$this->input->post('email'));
            //$this->sendemail($email,' Account Creation',$msg);
            $this->session_assign($username, $password);
            header('Refresh: 3; url=' . base_url('index.php/home/login/'));

        }
        $this->load->view('home/login/register', $data);
    }

    //this is session is assigned to the user after signing up an account , it is a one time function
    function session_assign($u, $p)
    {
        $results = $this->account_exists($u, $p);
        if (isset($results->username)) {
            //$this->db->insert('wallet',array('user'=>$results->id,'start_amount'=>0,'current_amount'=>0,'created_on'=>time(),'created_by'=>$results->id));

            $session_data = array(
                'id' => $results->id,
                'username' => $results->username,
                'fullname' => $results->full_name,
                'photo' => $results->photo,
                'email' => $results->email,
                'user_type' => $results->user_type,
                'sub_type' => $results->sub_type,
                'country' => $results->country,
                'phone' => $results->phone,
                'branch_id' => $results->branch_id
                //'verified'=>$results->verified

            );

            $signin = $this->session->set_userdata($session_data);
            $this->db->where('id', $this->session->userdata('id'))->update('users', array('status' => '1'));
            $code = $this->GetCode(10);
            $msg = "Dear " . $results->full_name . ", \n\r\n  Your account Verificacion Code is
             \n\r\n  Verification Code :  " . $code . " \n\r\n
             , Please follow the link and fill in your Code " . base_url("index.php/myportal/email_verification");
            //$this->sendemail($results->email,' Account Creation',$msg);
            $this->db->insert('account_verification', array('user_id' => $results->id, 'code' => $code, 'created_on' => time()));
            // header('Refresh: 3; url=' . base_url('index.php/myportal'));
            $signin == true ? redirect('/customer/', 'location') : $this->login();


        } else {
            $this->login();
        }

    }

    // This is the function getting code
    public function GetCode($length)
    {

        $codes = array();
        $chars = "01a2B3c4D5EF6G7H8I9jK";
        srand((double)microtime() * 1000000);
        $i = 0;
        $code = '';
        $serial = '';

        $i = 0;

        while ($i < $length) {
            $num = rand() % 10;
            $tmp = substr($chars, $num, 1);
            $serial = $serial . $tmp;
            $i++;
        }

        return $serial;

    }

    //sending a notification
    function notification($no, $msg, $name, $reciever = null)
    {
        //$newphrase = str_replace($healthy, $yummy, $phrase);
        $fno = '256' . substr($no, 1);
        //sending the message using the api first message is to the subscriber
        $sent = $this->SendSMS('Newspaper Management System', $fno, $msg);
        $dest = strlen($reciever) > 0 ? $reciever : $this->session->userdata('email');
        $this->sendemail($dest, '', $msg);//reciever, subject, message

        $msg2 = ucfirst($name) . " account " . $no . ' Has been Created on ' . date('d-m-Y H:i:s');
        //this is the message to the director
        $this->sendemail('info@octopus.ug', '', $msg2);
        //$sent= $this->SendSMS('Octopus',256776997711,$msg2);
        //$sent_admin=$this->SendSMS('Octopus',256703970431,$msg2);

        $msg = array(
            'dest_id' => $fno,
            'message' => $msg,
            'status' => 'user:' . $sent . ' admin1:' . $sent_admin,
            'no_of_msg' => '1',
            'type' => 'text'
        );
        $this->Octopusmodel->save_msg($msg);
    }
    // this is the funtion for the about us

    // this is the function for contact us

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('home/login', 'refresh');
    }

    //this is the error page
    public function error()
    {
        $this->load->view('404');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */