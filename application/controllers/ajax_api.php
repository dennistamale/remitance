<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class ajax_api extends CI_Controller {


    public $page_level;
    public $page_level2 = "";

    function __construct()
        {
            parent::__construct();

            $user_type = $this->session->userdata('user_type');

            switch ($user_type) {
                case 1:
                    $this->page_level = 'admin';
                    break;
                case 2:
                    $this->page_level = 'teller';
                    break;
                case 3:
                    $this->page_level = 'management';
                    break;
                case 4:
                    $this->page_level = 'accountant';
                    break;
                case 5:
                    $this->page_level = 'remitter';
                    break;
                case 6:
                    $this->page_level = 'branch_manager';
                    break;

            }

        }

    function country_branches($country)
        { ?>
            <option value="" <?php echo set_select('branch', '', TRUE); ?> >Select branch</option>
            <?php foreach ($this->db->select('id,branch_name')->from('branch')->where('country', $country)->get()->result() as $br) { ?>
            <option
                value="<?php echo $br->id ?>" <?php echo set_select('branch', $br->id); ?> ><?php echo $br->branch_name ?></option>
        <?php } ?>

            <?php

        }


    function inbox_notification(){
        $n= $this->db->where(array('replied'=>'N'))->from('inbox')->count_all_results();

        echo $n;

    }


    //this is the function which calculates the commission
    public function commission($dest, $amt, $sender_currency)
        {
            //checking if the source and destination are not the same

            $current_country = $this->session->userdata('country');
            $type = $dest == $current_country ? 'local' : 'foreign';
            //selecting the data for the specified commission
            $amount = $this->forex_rate($sender_currency, 'USD') * $amt;

            $commission = $this->db
                ->query("SELECT unit_value,units FROM commission WHERE type='$type' AND country='$current_country' AND $amount BETWEEN min AND max LIMIT 1 ;")
                ->row();

            if (isset($commission->unit_value)) {

                if ($commission->units == 'percent') {
                    $com = ($commission->unit_value / 100) * $amount;
                    echo $com;
                }
                else {

                    $com = $commission->unit_value;
                    echo $com;
                };


            }
            else {

                echo 0;

            }


        }

    //this is the function which calculates the commission


    //this is the function which get the tax percentages for specific country

    public function forex_rate($from_currency, $convert_currency)
        {
            if ($from_currency == $convert_currency) {
                return 1;
            }
            elseif ($from_currency != $convert_currency) {
                $forex = $this->db->select('to_amount')->from('forex_settings')
                    ->where(array(
                        'country' => $this->session->userdata('country'),
                        'from_currency' => $from_currency,
                        'to_currency' => $convert_currency
                    ))->get()->row();
                return isset($forex->to_amount) ? $forex->to_amount : 1;
            }

        }

//this is the function which works for the forex rates for the system

    public function tax_percentage($dest = null, $commission = null, $taxed = 'commission')
        {


            if (isset($dest) && isset($commission)) {

                $current_country=$this->session->userdata('country');
                $type = $dest == $current_country ? 'local' : 'foreign';

                $tax = $this->db->select()->from('taxes')->where(array('country' => $current_country, 'taxed' => $taxed, 'type' => $type))->get()->row();

                if (count($tax) == 1) {


                    $tax_percentage = $tax->unit_value / 100;
                    echo $tax = $commission * $tax_percentage;

                    //$final_tax=$commission-$tax;


                }
                else {
                    echo 0;
                }
            }
            else {
                echo 0;
            }

        }

    public function reverse_commission($amt, $dest, $sender_currency)
        {
            //checking if the source and destination are not the same

            $current_country = $this->session->userdata('country');
            $type = $dest == $current_country ? 'local' : 'foreign';
            //selecting the data for the specified commission
            $amount = $this->forex_rate($sender_currency, 'USD') * $amt;


            $commission = $this->db
                ->query("SELECT unit_value,units FROM commission WHERE type='$type' AND country='$current_country' AND $amount BETWEEN min AND max LIMIT 1 ;")
                ->row();

            if (isset($commission->unit_value)) {

                if ($commission->units == 'percent') {

                    /* var p=(net_amount.value*100)/(100-commission.value);
                    amount.value=parseFloat(p).toFixed(2);*/
                    //Am=Na/(1-%com);
                    $p_comm = $commission->unit_value / 100;

                    $c_amt = $gross_amount = $amount * (1 - $p_comm);

                    $tt_comm = $amount - $c_amt;

                    $gross_amount = $amount + $tt_comm;
                     $com = $this->forex_rate('USD', $sender_currency) * $gross_amount;


                    echo $com>=0?$com:0;

                }
                else {
                    $original_amount = $amount + $commission->unit_value;

                    //converting the amount to local currency again
                    $com = $this->forex_rate('USD', $sender_currency) * $original_amount;


                    echo $com>=0?$com:0;
                };


            }
            else {

                echo 0;

            }


        }

    public function country_rates($dest, $branch_id)
        {

            //checking if the source and destination are not the same

            $current_country = $this->session->userdata('country');
//        $current_branch=$this->session->userdata('branch_id');
            $type = $dest == $current_country ? 'local' : 'foreign';
            //selecting the data for the specified commission
//        AND country='$current_country'

            $qry = $this->db->query("SELECT * FROM commission WHERE type='$type'  AND country='$current_country' ORDER BY min ASC ;");
            $rate = $qry->result();


            if ($qry->num_rows() == 0) {
                echo '<div class="note note-info"style="margin: 0;">No Rates found for Specified Country and Branch</div>';
            }
            else {

                ?>

                <div class="note note-info" style="margin: 0; padding: 0px;">
                    <table class="table table-striped table-bordered table-hover" style="bottom: 0;">

                        <thead>
                        <tr>
                            <th>Type</th>
                            <th> Min</th>
                            <th> Max</th>
                            <th>Units</th>
                            <th> USD</th>

                        </tr>
                        </thead>
                        <tbody>

                        <?php


                        foreach ($rate as $c) { ?>

                            <tr>
                                <td><?php echo ucwords($c->type) ?></td>
                                <td> <?php echo number_format($c->min) ?> </td>
                                <td> <?php echo $c->max == 10000000 ? '*' : $c->max ?> </td>
                                <td><?php echo $c->units == 'amount' ? 'Amt' : 'Per' ?></td>
                                <td> <?php echo $c->unit_value ?> </td>


                            </tr>

                            <?php
                        } ?>

                        </tbody>
                    </table>
                </div>

            <?php }
        }


    public function city($country)
        {


            $this->db->select('state,country,title')->from('state')->order_by('country', 'asc')->where('country', $country);
            $ct = $this->db->order_by('country', 'asc')->get()->result(); ?>

            <option value="" <?php echo set_select('city', '', TRUE); ?> >Select City</option>
            <?php

            foreach ($ct as $city) { ?>
                <option
                    value="<?php echo $city->state ?>" <?php echo set_select('city', $city->state); ?> ><?php echo $city->title ?></option>
            <?php }

        }

    function branches($country)
        {

            $branch = $this->db->select()->from('branch')->where('country', $country)->get()->result();
            foreach ($branch as $to) {

                echo ' <option value="' . $to->id . '" >' . $to->branch_name . '</option>';


            }
        }

    function users()
        {

            /*
             * Paging
             */

            //var_dump($_POST["selected"]);

            $iTotalRecords = $this->db->where(array('id !=' => $this->session->userdata('id')))->from('users')->count_all_results();
            $iDisplayLength = intval($_REQUEST['length']);
            $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
            $iDisplayStart = intval($_REQUEST['start']);
            $sEcho = intval($_REQUEST['draw']);

            $records = array();
            $records["data"] = array();

            $end = $iDisplayStart + $iDisplayLength;
            $end = $end > $iTotalRecords ? $iTotalRecords : $end;


            $users = $this->db->select('id, full_name, city, country, phone,  gender,dob,branch_id,user_type,status')->from('users')
                ->where(array('id !=' => $this->session->userdata('id')))->limit($end, $iDisplayStart)->get()->result();


            $no = 1;
            foreach ($users as $c):


                $action = '<li>';
                $action .= anchor($this->page_level . '/users/edit/' . $c->id * date('Y'), '  <i class="fa fa-pencil"></i> Edit');
                $action .= '</li>';
                if ($this->session->userdata('user_type') != '2') {
                    $action .= '<li>';
                    $action .= anchor($this->page_level . '/users/delete/' . $c->id * date('Y'), '  <i class="fa fa-trash-o"></i> Delete', 'onclick="return confirm(\'Are you sure you want to delete ?\')"');
                    $action .= '</li>';

                    $action .= '<li>';
                    $action .= $c->status == 2 ? anchor($this->page_level . '/users/unblock/' . $c->id * date('Y'), '  <i class="fa fa-check"></i> Unblock') : anchor($this->page_level . '/users/ban/' . $c->id * date('Y'), '  <i class="fa fa-ban"></i> Ban', 'onclick="return confirm(\'You are about to ban User from accessing the System \')"');
                    $action .= '</li>';

                    $action .= '<li>';
                    $action .= anchor($this->page_level . '/users/make_admin/' . $c->id * date('Y'), '  <i class="i"></i> Make admin');
                    $action .= '</li>';
                }


                //selecting for the country
                $ct = $this->db->select('country')->from('country')->where('a2_iso', $c->country)->get()->row();
                //selecting for the branch_name
                $br = $this->db->select('branch_name')->from('branch')->where('id', $c->branch_id)->get()->row();
                $branch_name = isset($br->branch_name) ? $br->branch_name : 'N/A';

                //user type
                $ut = $this->db->select('title')->from('user_type')->where('id', $c->user_type)->get()->row();


                $records["data"][] = array(

                    '<input type="checkbox" name="id[]" value="' . $c->id . '">',

                    $no,
                    $c->full_name,

                    date('d-m-Y', $c->dob),

                    $ct->country,
                    $c->city,
                    $c->phone,
                    $branch_name,
                    $ut->title,

                    '
                 <div class="btn-group">
                                <a class="btn green-jungle btn-sm" href="javascript:;" data-toggle="dropdown">
                                    <i class="fa fa-cogs"></i> Action <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">

' .
                    $action
                    . '

                                </ul>
                            </div>
                '

                );
                $no++; endforeach;

            if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }

            $records["draw"] = $sEcho;
            $records["recordsTotal"] = $iTotalRecords;
            $records["recordsFiltered"] = $iTotalRecords;

            echo json_encode($records);
        }


    function check_log()
        {

            $status = '';


            $log = $this->db->select('trader_id,name,phone,company,FROM_UNIXTIME(`created_on`)  as time')->from('check_on')->order_by('id', 'desc')->get()->result();


            foreach ($log as $ch => $s):

                $u[$ch]['name'] = humanize($s->name);
                $u[$ch]['phone'] = $s->phone;
                $u[$ch]['company'] = $s->company;
                $u[$ch]['time'] = $s->time;

                $sum_paid = $this->db->select('`check_status as status`,FROM_UNIXTIME(`created_on`)  as time')->from('check_on')->where(array('trader_id' => $s->trader_id))->order_by('id', 'desc')->get()->result();

                $u[$ch]['log_details'] = $sum_paid;

            endforeach;

            $response = array(
                'response' => 'success',
                'log' => $u,


            );

            header('Content-Type:application/json');
            echo json_encode($response);
        }


    function trans($fdate = '2016-08-01')
        {


//echo $rfc_1123_date = gmdate('D, d M Y H:i:s T', time());


            $begin = new DateTime($fdate);
            $ldate = isset($last_day) ? date($last_day) : date('Y-m-d');
            $end = new DateTime($ldate);

            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($begin, $interval, $end->modify('+1 day'));


            foreach ($period as $s => $dt):
                $a = $this->db->select('sum(sent_amount_usd) as amt')->from('transactions')->like('DATE(FROM_UNIXTIME(created_on))', $dt->format("y-m-d"), 'before')->get()->row();

                $u[$s] = date('D, d M Y H:i:s T', strtotime($dt->format("y-m-d")));
//       $u[$s]['y'] = round($a->amt,2);


            endforeach;


//        $response = array(
//
//            'data' => $u
//
//
//        );

            header('Content-Type:application/json');
            echo json_encode($u);
            // echo $_GET['callback']. '('. json_encode($u) . ')';
        }




    function bulk_sms($phone, $message)
        {
            $phones = explode('_', $phone);

            if (count($phones) > 0) {
                foreach ($phones as $t) {
                    //$this->send_sms(phone($t), urldecode($message));
                }
            }
        }


    function message_form($user_id = '', $send_m = 0)
        {

            if ($send_m == 0) {
                if (strlen($user_id) > 0) {
                    $p = $this->db->select('phone,first_name')->from('users')->where('id', $user_id / date('Y'))->get()->row();

                    if (count($p) > 0) {
                        $phone = phone($p->phone);
                        $first_name = $p->first_name;
                    }
                    else {
                        $phone = '';
                        $first_name = '';
                    }
                }
                else {
                    $phone = '';
                    $first_name = '';
                }

                echo '
<link href="' . base_url() . 'assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />
        <div class="modal-header" >
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <h4 class="modal-title">Send <b>' . humanize($first_name) . '</b> Message</h4>
</div>
<div class="modal-body">
    <div class="row">
       <form action="#" class="form-horizontal">
        <div class="col-md-12"   id="body" >




                                                    <div class="form-body">

                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Phone</label>
                                                            <div class="col-md-10">
                                                                <div class="input-group">


                                                                    <input type="text" class="form-control" id="phone" name="phone" value="' . $phone . '" data-role="tagsinput">
                                                                     </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <label class="col-md-2 control-label">Message</label>
                                                            <div class="col-md-10">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">
                                                                        <i class="fa fa-envelope"></i>
                                                                    </span>
                                                                    <textarea name="message" max="150" required id="message" rows="5" class="form-control" >
                                                                     </textarea>
                                                                     </div>
                                                            </div>
                                                        </div>
                                                    </div>


        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn default" data-dismiss="modal">Close</button>
    <button type="button" class="btn blue" id="send_message">Send Message</button>

</div>
 </form>
<script src="' . base_url() . 'assets/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
  ';
            }
            elseif ($send_m == 1) {
                $data = array(
                    'alert' => 'success',
                    'message' => ' Message has been sent successfully'
                );
                $this->load->view('alert', $data);
            }
            ?>


            <script>


                $("#send_message").click(function () {

                    var p = $('#phone').val();
                    p = p.replace(/,/g, "_")
                    var m = $('#message').val();
                    var mm = m.trim();

                    if (p != '' && mm != '') {

                        $.ajax({
                            type: 'GET',
                            url: '<?php echo base_url("index.php/ajax_api/bulk_sms")?>/' + p + '/' + mm,
                            beforeSend: function () {
                                $("#body").html('<div class="note note-info note-shadow"><i class="fa fa-circle-o-notch fa-spin" ></i> Sending Message Please Wait....</div>');
                            },
                            success: function (d) {

                                $("#body").html('<div class="note note-success note-shadow"><i class="fa fa-check" ></i> Message Sent successfully</div>');


                            }


                        });
                    }


                })
            </script>

            <?php


        }



    function send_sms($phone, $sms)
        {
            if (!empty($phone)) {
                if (strstr($phone, ',') or strstr($phone, '/') or strstr($phone, '-')) {
                    if (strstr($phone, ',')) {
                        $phone = substr($phone, 0, strpos($phone, ','));
                    }
                    if (strstr($phone, '/')) {
                        $phone = substr($phone, 0, strpos($phone, '/'));
                    }
                    if (strstr($phone, '-')) {
                        $phone = substr($phone, 0, strpos($phone, '-'));
                    }
                    $phone = trim($phone);
                }
                if (strlen($phone) >= 9) {
                    if (substr($phone, 0, 1) == '0') {
                        if (strlen($phone) == 10) {
                            $phone = '256' . substr($phone, 1);
                        }
                    }
                    if (strlen($phone) == 9 and substr($phone, 0, 1) !== 0) {
                        $phone = '256' . $phone;
                    }
                    if (substr($phone, 0, 1) == '+') {
                        $phone = substr($phone, 1);
                    }
                    $q = $this->db->select('id')->from('outbox')->where(array('to_user' => $phone, 'message' => $sms))->limit(1)->get()->result();
                    if (!isset($q[0]->id)) {
                        $request = "";
                        $request .= urlencode("Mocean-Username") . "=" . urlencode("mixa") . "&";
                        $request .= urlencode("Mocean-Password") . "=" . urlencode("0n6cAd") . "&";
                        $request .= urlencode("Mocean-From") . "=" . urlencode("remit") . "&";
                        $request .= urlencode("Mocean-To") . "=" . urlencode($phone) . "&";
                        $request .= urlencode("Mocean-Url-Text") . "=" . urlencode($sms);
                        // Build the header
                        $host = "sms.smsone.co.ug";
                        $script = "/cgi-bin/sendsms";
                        $request_length = 0;
                        $method = "POST";
                        //Now construct the headers.
                        $header = "$method $script HTTP/1.1\r\n";
                        $header .= "Host: $host\r\n";
                        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
                        $header .= "Content-length: " . strlen($request) . "\r\n";
                        $header .= "Connection: close\r\n\r\n";
                        // Open the connection
                        $port = 8866;
                        $socket = @fsockopen($host, $port, $errno, $errstr);
                        if ($socket) {
                            // Send HTTP request
                            fputs($socket, $header . $request);
                            // Get the response
                            while (!feof($socket)) {
                                $output[] = fgets($socket); //get the results
                            }
                            fclose($socket);
                            //$this->db->insert('outbox','to_user,subject,m_type,message,created_on,created_by',"'$phone','Notification','SMS','$sms','".time()."','0'");
                            $this->db->insert('outbox', array('to_user' => $phone, 'subject' => 'Mixakids', 'm_type' => 'SMS', 'message' => $sms, 'created_on' => time(), 'created_by' => '0'));
                        }
                    }
                }
            }
        }






}

?>