<?php
class model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }
        function add_data($values,$table){
            $this->db->insert($table,$values);
        }
//these are the custom queries
    function select($values=null,$table=null,$where=null,$like=null,$limit=null){

        $this->db->select($values)->from($table);
        $where>0?$this->db->where($where):'';
        $like>0?$this->db->like($like):'';
        $limit>0?$this->db->limit($limit):'';
        $this->db->order_by('id','desc');
        return $this->db->get()->result();

    }
    // this function inserts data into the database
    function insert($table,$values){
        $this->db->insert($table,$values);
    }
    // this is a function for updating data
    function update($table,$values,$unique){
        $this->db->where($unique)->update($table,$values);
    }

         function results($username, $pass)
    {

        $this->db->select('');
        $this->db->from('users');
        //you can either use this method down
        $where = array('username' => $username, 'password' => $pass);
        $this->db->where($where);
        $user = $this->db->get();
        
        return $user->result();
    }
    //inserting user
    function update_status($id, $status)
    {
        $values = array('status' => $status);

        $this->db->where('id', $id);
        $this->db->update('users', $values);

    }
    //deleting the data from tha database
    function delete($table,$id)
    {
      $this->db->where('id',$id);
      $this->db->delete($table);
      
        
    }
    function profile()
{
    $this->db->where('id',$this->session->userdata('id'));
    $this->db->select('*');
    $this->db->from('users');
    $prof=$this->db->get();
    return $prof->result();
    
    
}
function update_profile($pro)
{
    
    if(strlen($pro['newpass'])>0){$pass=$pro['newpass'];}else{$pass=$pro['cpass'];}
    $pr=array(
    'fname'=>$pro['fname'],
    'lname'=>$pro['lname'],
    'email'=>$pro['email'],
    'password'=>sha1($pass)
    );
    $this->db->where('id',$this->session->userdata('id'));
    $this->db->update('users',$pr);
    
}
function p_check($str)
{
    $dat=array('id'=>$this->session->userdata('id'),'password'=>sha1($str));
    $this->db->where($dat);
    $this->db->select('*');
    $this->db->from('users');
    $result=$this->db->get();
    return $result->result();
}


}